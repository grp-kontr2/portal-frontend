import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Secret, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SecretService } from '../../services/secret.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit, OnDestroy {

  @Input() loggedInUser: User;
  @Input() secret: Secret;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private flash: FlashService,
              private service: SecretService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doDelete() {
    const secretId = this.secret.id;
    console.log('have secret', this.secret);
    this.subscriptions.push(this.service.deleteSecret(this.secret.client.id, secretId).subscribe(
      () => {
        console.log(`Deleted secret ${secretId}`);
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['.'], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess(`Deleted secret '${this.secret.name}'`);
        });

      }, (error: any) => {
        this.flash.flashDanger(`Delete unsuccessful: ${error.error}`);
        this.flash.handleError(error);
      }
    ));
  }
}
