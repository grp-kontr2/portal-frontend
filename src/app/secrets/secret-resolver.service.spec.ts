import { TestBed, inject } from '@angular/core/testing';

import { SecretResolver } from './secret-resolver.service';

describe('SecretResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecretResolver]
    });
  });

  it('should be created', inject([SecretResolver], (service: SecretResolver) => {
    expect(service).toBeTruthy();
  }));
});
