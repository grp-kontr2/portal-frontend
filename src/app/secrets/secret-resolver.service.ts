import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Client, Secret } from '../shared/models/models';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SecretService } from '../services/secret.service';
import { FlashService } from '../services/flash.service';

@Injectable({
  providedIn: 'root'
})
export class SecretResolver implements Resolve<Secret> {

  constructor(private service: SecretService,
              private router: Router,
              private flash: FlashService) {
  }

  private getOwner(route: ActivatedRouteSnapshot): Client {
    const user = route.parent.data.get( 'user' ) as Client;
    const worker = route.parent.data.get( 'worker' ) as Client;

    if (user == null && worker == null) {
      this.flash.flashDanger(`Parent route for secret does not contain possible owner.`); // todo: can this cast correctly?
      return null;
    }
    return user == null ? worker : user;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Secret> {
    const sid = route.paramMap.get('sid');

    const owner = this.getOwner(route);

    return this.service.loadSecret(owner.id, sid).pipe(take(1), map(secret => {
      if (secret) {
        return secret;
      } else {
        this.router.navigateByUrl('/').then(
          () => {
            this.flash.flashDanger(`Secret ${secret} not found.`);
          }, (error: any) => {
            console.error(`[RESOLVE] Could not navigate to secret ${sid}: `, error.error);
            this.flash.handleError(error);
          });
        return null;
      }
    }));
  }
}
