import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecretListComponent } from './secret-list/secret-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SecretResolver } from './secret-resolver.service';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { SecretCreateComponent } from './secret-create/secret-create.component';
import { SecretsRoutingModule } from './secrets-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SecretsRoutingModule
  ],
  providers: [
    SecretResolver
  ],
  declarations: [ SecretListComponent, DeleteModalComponent, SecretCreateComponent],
  exports: [
    SecretListComponent
  ]
})
export class SecretsModule { }
