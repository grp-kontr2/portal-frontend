import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../shared/models/models';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { ManagementService } from '../../services/management.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: [ './base-layout.component.scss' ]
})
export class BaseLayoutComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  subscriptions: Subscription[] = [];
  status: boolean;

  constructor(private auth: AuthService,
              private route: ActivatedRoute,
              private managementService: ManagementService,
              private flash: FlashService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
    }));
    this.subscriptions.push(this.getStatusPeriodically());
  }

  private getStatus() {
    this.subscriptions.push(this.managementService.getPortalStatus().subscribe((status) => {
      console.log(`[STATUS] Status:`, status);
      this.status = status.ok;
      if (!this.status) {
        this.flash.flashDanger('Backend is not running - please contact the administrator!',
          { timeout: 20000 });
      }
    }, (err) => { this.status = false; this.flash.handleError(err); }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private getStatusPeriodically() {
    return timer(0, 20000).subscribe(() => this.getStatus());
  }
}
