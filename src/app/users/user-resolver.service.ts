import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { User } from '../shared/models/models';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';

import { map, take } from 'rxjs/operators';
import { FlashService } from '../services/flash.service';

@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private service: UserService, private router: Router, private flash: FlashService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const uid = route.paramMap.get('uid');

    return this.service.getUser(uid).pipe(take(1), map(user => {
        if (user) {
          return user;
        } else {
          this.router.navigateByUrl('/').then(
            () => {
              this.flash.flashDanger(`User ${uid} not found.`);
            }, (error: any) => {
              this.flash.flashDanger('Could not navigate home');
              this.flash.handleError(error);
            });
          return null;
        }
      })
    );
  }
}
