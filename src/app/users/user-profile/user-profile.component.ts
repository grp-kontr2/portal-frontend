import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../shared/models/models';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: [ './user-profile.component.scss' ]
})
export class UserProfileComponent implements OnInit, OnDestroy {

  user: User; // the user whose profile is being viewed
  loggedInUser: User; // the user who is viewing the profile
  own: boolean;
  updateData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private service: UserService,
              public auth: AuthService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private flash: FlashService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { user: User, loggedInUser: User }) => {
      this.user = data.user;
      this.loggedInUser = data.loggedInUser;
      this.own = (this.user.id === this.loggedInUser.id);
      this.createForm();
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('setting values for user profile form');
    const makeDate = (date) => {
      return this.datePipe.transform(date, 'yyyy-MM-ddTHH:mm');
      ;
    };

    this.updateData.setValue({
      username: this.user.username,
      gitlab_username: this.user.gitlab_username,
      email: this.user.email,
      uco: this.user.uco,
      name: this.user.name,
      is_admin: this.user.is_admin,
      created_at: makeDate(this.user.created_at),
      updated_at: makeDate(this.user.updated_at),
    });
  }

  createForm() {
    const makeDate = () => {
      return this.fb.control({ value: undefined, disabled: true });
    };

    const _this = this;

    const _fe = () => {
      return { value: '', disabled: !_this.canUpdate() };
    };

    this.updateData = this.fb.group({
      username: this.fb.control({ value: '', disabled: !this.loggedInUser.is_admin }, Validators.required),
      email: this.fb.control(_fe(), Validators.required),
      uco: this.fb.control(_fe()),
      gitlab_username: this.fb.control(_fe()),
      name: this.fb.control(_fe()),
      is_admin: this.fb.control(_fe()),
      created_at: makeDate(),
      updated_at: makeDate(),
    });
  }

  updateProfile() {
    console.log('updating user profile');
    if (this.updateData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.updateData.value;
    const newValues = {
      id: this.user.id,
      username: data[ 'username' ],
      email: data[ 'email' ],
      uco: data[ 'uco' ],
      name: data[ 'name' ],
      is_admin: data[ 'is_admin' ]
    } as User;
    this.subscriptions.push(this.service.updateUser(newValues).subscribe(
      res => {
        this.ngOnInit();
        this.flash.flashSuccess('Update successful.');
        console.log('Result: ', res);
      }, (error: any) => {
        this.flash.flashDanger(`Update unsuccessful: ${newValues.id}`);
        this.flash.handleError(error);
      }
    ));
  }

  deleteUser() {
    const userId = this.user.id;
    this.subscriptions.push(this.service.deleteUser(userId).subscribe(
      res => {
        console.log('Deleted user ', userId);
        this.router.navigateByUrl('/users').then(() => {
          this.flash.flashSuccess(`Deleted user ${userId}`);
        });

      }, (error: any) => {
        this.flash.flashDanger(`Delete unsuccessful: ${userId}`);
        this.flash.handleError(error);
      }));
  }

  resetForm() {
    this.updateData.reset();
    this.initFormValues();
  }

  private canUpdate(): boolean {
    if (this.loggedInUser.is_admin) {
      return true;
    }

    if (this.loggedInUser.id !== this.user.id) {
      return false;
    }

    return !this.user.managed;
  }
}
