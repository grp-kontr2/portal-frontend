import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../shared/models/models';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-user-password',
  templateUrl: './user-password.component.html',
  styleUrls: ['./user-password.component.scss']
})
export class UserPasswordComponent implements OnInit {

  user: User; // user whose password is being changed
  loggedInUser: User; // user performing the password change
  passwordForm: FormGroup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private service: UserService,
              public auth: AuthService,
              private fb: FormBuilder,
              private flash: FlashService) {
    this.createForm();
  }

  ngOnInit() {
    this.route.data.subscribe((data: { user: User, loggedInUser: User }) => {
      this.user = data.user;
      this.loggedInUser = data.loggedInUser;
      if (this.loggedInUser.is_admin) {
        this.passwordForm.get('oldPassword').clearValidators();
        this.passwordForm.get('oldPassword').updateValueAndValidity();
        console.log('Removed validators from oldPassword.');
      }
    });
  }

  createForm() {
    this.passwordForm = this.fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      newPasswordAgain: ['', Validators.required]
    });
  }

  changePassword() {
    console.log('changing password from ui form for user ', this.user);
    if (this.passwordForm.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.passwordForm.value;
    const oldPassword = data['oldPassword'];
    const newPassword = data['newPassword'];

    this.service.changeUserPassword(this.user.id, oldPassword, newPassword).subscribe(
      res => {
        console.log('response from user password change: ', res);
        this.router.navigateByUrl('/users/' + this.user.id).then(() => {
          this.flash.flashSuccess('Password update successful.');
        });
      }, (error: any) => {
        this.flash.flashDanger(`Password change unsuccessful: ${error.error}`);
        this.passwordForm.reset();
      });
  }

  checkMatchingPasswords() {
    return this.passwordForm.value['newPassword'] === this.passwordForm.value['newPasswordAgain'];
  }

  get newPassword() {
    return this.passwordForm.get('newPassword');
  }

  get newPasswordAgain() {
    return this.passwordForm.get('newPasswordAgain');
  }
}
