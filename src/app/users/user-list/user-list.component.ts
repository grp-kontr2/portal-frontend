import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/models/models';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { UserService } from '../../services/user.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: [ './user-list.component.scss' ]
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  temp: User[];
  subscriptions: Subscription[] = [];

  loadingIndicator = false;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private route: ActivatedRoute,
              private usersService: UserService,
              private flash: FlashService) {
  }

  ngOnInit() {
    this.loadingIndicator = true;
    this.listUsers();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private usersReceived(users) {
    this.users = users;
    this.temp = [ ...users ];
    this.loadingIndicator = false;
    this.flash.loadResources(users, 'users');
  }

  listUsers() {
    this.loadingIndicator = true;
    this.subscriptions.push(this.usersService.listUsers().subscribe((users) => this.usersReceived(users)
      , (err) => this.flash.handleError(err)));
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.users = this.temp.filter(function (d) {
      return d.username.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
