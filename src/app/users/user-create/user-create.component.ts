import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {UserService} from '../../services/user.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  createData: FormGroup;
  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private flash: FlashService) {
    this.createForm();
  }

  createForm() {
    this.createData = this.fb.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      uco: ['', Validators.required],
      is_admin: [false],
    });
  }

  ngOnInit() { }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('User create form data: ', data);
    this.userService.createUser(data['username'], data['name'], data['email'], data['uco'], data['is_admin']).subscribe(
      () => {
      this.router.navigateByUrl('/users').then(() => {
        this.flash.flashSuccess(`Created user ${data['username']}.`);
      });
    }, (error: any) => {
        console.log('Error creating user: ', error);
        this.flash.flashDanger(`Error creating user ${data['username']}.`);
        this.flash.handleError(error);
      });
  }
}
