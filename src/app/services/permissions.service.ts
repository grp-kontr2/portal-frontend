import { Injectable } from '@angular/core';
import { Course, User } from '../shared/models/models';

@Injectable()
export class PermissionsService {

  constructor() {

  }

  public checkReadProjects(user: User, course: Course) {
    const required = [ 'write_projects' ];
    return this.checkViewCourseFull(user, course) || this.checkAny(user, course, required);
  }


  public checkReadResults(user: User, course: Course) {
    const required = [ 'read_all_submission_files' ];
    return this.checkViewCourseFull(user, course) || this.checkAny(user, course, required);
  }

  public checkReadGroups(user: User, course: Course) {
    const required = [ 'write_groups' ];
    return this.checkViewCourseFull(user, course) || this.checkAny(user, course, required);
  }

  public checkReadRoles(user: User, course: Course) {
    const required = [ 'write_roles' ];
    return this.checkViewCourseFull(user, course) || this.checkAny(user, course, required);
  }

  public checkUpdateCourse(user: User, course: Course) {
    return this.checkAll(user, course, [ 'update_course' ]);
  }

  public checkWriteRoles(user: User, course: Course) {
    const required = [ 'write_roles' ];
    return this.checkUpdateCourse(user, course) || this.checkAny(user, course, required);
  }

  public checkWriteProjects(user: User, course: Course) {
    const required = [ 'write_projects' ];
    return this.checkUpdateCourse(user, course) || this.checkAny(user, course, required);
  }

  public checkWriteGroups(user: User, course: Course) {

    const required = [ 'write_groups' ];
    return this.checkUpdateCourse(user, course) || this.checkAny(user, course, required);
  }

  public checkReviewSubmission(user: User, course: Course): boolean {
    const required = [ 'write_reviews_all', 'write_reviews_group' ];
    return this.checkAny(user, course, required);
  }

  public checkAny(user: User, course: Course | string, required: string[]): boolean {
    if (user == null) {
      return false;
    }

    if (user.is_admin) {
      return true;
    }

    if (course == null) {
      return false;
    }

    const provided = (typeof course === 'string') ? course : user.permissions[ course.id ];
    console.log('provided permissions: ', provided);
    for (const permissionName of required) {
      if (provided[ permissionName ] === true) {
        console.log(`[PERM] Satisfied: ${permissionName}`);
        return true;
      }
    }
    console.log(`[PERM] No satisfied of ${required}`);
    return false;
  }

  public checkAll(user: User, course: Course | string, required: string[]): boolean {
    if (user == null) {
      return false;
    }

    if (user.is_admin) {
      return true;
    }

    if (course == null) {
      return false;
    }

    const provided = (typeof course === 'string') ? course : user.permissions[ course.id ];
    console.log('provided permissions: ', provided);
    for (const permissionName of required) {
      if (provided[ permissionName ] !== true) {
        console.log(`[PERM] Not satisfied: ${permissionName}`);
        return false;
      }
    }
    console.log(`[PERM] All satisfied of ${required}.`);
    return true;
  }

  public checkCreateSubmission(user: User, course: Course) {
    return this.checkAny(user, course, [ 'create_submissions', 'create_submissions_other' ]);
  }

  public checkCreateSubmissionOther(user: User, course: Course) {
    return this.checkAny(user, course, [ 'create_submissions_other' ]);
  }

  public checkViewCourseFull(user: User, course: Course) {
    const required = [ 'view_course_full' ];
    return this.checkUpdateCourse(user, course) || this.checkAny(user, course, required);
  }

  public checkViewCourseLimited(user: User, course: Course) {
    const required = [ 'view_course_limited' ];
    return this.checkUpdateCourse(user, course) || this.checkAny(user, course, required);
  }
}

