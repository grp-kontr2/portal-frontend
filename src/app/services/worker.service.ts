import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Worker } from '../shared/models/models';
import { Observable } from 'rxjs';

@Injectable()
export class WorkerService {
  workersUrl = `${environment.baseUrl}/api/v1.0/workers`;

  constructor(private http: HttpClient) {
  }

  public listWorkers(): Observable<Worker[]> {
    const url = this.workersUrl;
    console.log(`[WORKER] List (${url})`);
    return this.http.get<Worker[]>(url);
  }

  public getWorker(id: string): Observable<Worker> {
    const url = `${this.workersUrl}/${id}`;
    console.log(`[WORKER] Find (${url})`);
    return this.http.get<Worker>(url);
  }

  public createWorker(data) {
    const url = this.workersUrl;
    const body = {
      'name': data['name'],
      'tags': data['tags'],
      'portal_secret': data['portalSecret'],
      'url': data['url'],
    };
    console.log(`[WORKER] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }

  public updateWorker(worker: Worker) {
    const url = `${this.workersUrl}/${worker.id}`;
    const body = {
      'name': worker.name,
      'portal_secret': worker.portal_secret,
      'url': worker.url,
      'tags': worker.tags,
    };
    console.log(`[COMPONENT] Update (${url}): ${body}`);
    return this.http.put(url, body);
  }

  public deleteWorkers(id: string) {
    const url = `${this.workersUrl}/${id}`;
    console.log(`[WORKER] Delete (${url})`);
    return this.http.delete(url);
  }

  getWorkerStatus(id: string) {
    const url = `${this.workersUrl}/${id}/status`;
    return this.http.get(url);
  }
}
