import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group, Project, Role } from '../shared/models/models';
import { environment } from '../../environments/environment';

@Injectable()
export class GroupService {

  constructor(private http: HttpClient) {
  }

  coursesUrl = `${environment.baseUrl}/api/v1.0/courses`;


  public findGroups(courseId: string): Observable<Group[]> {
    const url = `${this.coursesUrl}/${courseId}/groups`;
    console.log(`[GROUP] Find (${url})`);
    return this.http.get<Group[]>(url);
  }

  public findGroup(courseId: string, groupId: string): Observable<Group> {
    const url = `${this.coursesUrl}/${courseId}/groups/${groupId}`;
    console.log(`[GROUP] Find (${url})`);
    return this.http.get<Group>(url);
  }

  public createGroup(courseId: string, name: string, description: string, codename: string) {
    const url = `${this.coursesUrl}/${courseId}/groups`;
    const body = {
      name: name,
      description: description,
      codename: codename
    };
    console.log(`[GROUP] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }

  public updateGroup(group: Group) {
    const url = `${this.coursesUrl}/${group.course.id}/groups/${group.id}`;
    const body = {
      name: group.name,
      codename: group.codename,
      description: group.description,
    };
    console.log(`[GROUP] Update (${url}): ${body} `);
    return this.http.put(url, body);
  }

  public deleteGroup(courseId: string, groupId: string) {
    const url = `${this.coursesUrl}/${courseId}/groups/${groupId}`;
    console.log(`[GROUP] Delete (${url})`);
    return this.http.delete(url);
  }

  public removeUser(group: Group, userId: string) {
    const url = `${this.coursesUrl}/${group.course.id}/groups/${group.id}/users/${userId}`;
    console.log(`[GROUP] Remove user (${url})`);
    return this.http.delete(url);
  }

  public addUser(group: Group, userId: string) {
    const url = `${this.coursesUrl}/${group.course.id}/groups/${group.id}/users/${userId}`;
    console.log(`[GROUP] Add user (${url})`);
    return this.http.put(url, {});
  }

  public addProject(group: Group, projectId: string) {
    const url = `${this.coursesUrl}/${group.course.id}/groups/${group.id}/projects/${projectId}`;
    console.log(`[GROUP] Add project (${url})`);
    return this.http.put(url, {});
  }

  public removeProject(group: Group, projectId: string) {
    const url = `${this.coursesUrl}/${group.course.id}/groups/${group.id}/projects/${projectId}`;
    console.log(`[GROUP] Remove project (${url})`);
    return this.http.delete(url);
  }
}
