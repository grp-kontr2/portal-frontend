import { Injectable } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable({
  providedIn: 'root'
})
export class FlashService {

  constructor(private flashMsg: FlashMessagesService) {
  }

  static getErrorMessage(err): string {
    const error = err.error;
    const err_text = 'Error occurred: ';
    if (!error) {
      return `${err_text} ${err}`;
    }
    if (error[ 'message' ]) {
      return `${err_text} ${error[ 'message' ]}`;
    } else {
      return `${err_text} ${error}`;
    }
  }

  public loadResources(resources: any[], entity: string) {
    const text = `Loading ${entity}: ${resources.length} loaded ${entity}`;
    console.log(`[LOAD] ${text}`, resources);
    this.flashSuccess(text);
  }

  public flashLoadResource(resource: any, entity: string) {
    const text = `Loading ${entity}: ${resource} loaded ${entity}`;
    console.log(`[LOAD] ${text}`, resource);
    this.flashSuccess(text);
  }

  public handleError(err) {
    const err_text = FlashService.getErrorMessage(err);
    console.log(`[ERR] ${err_text}`, err);
    this.flashDanger(err_text);
  }

  public flashSuccess(text: string, options?: {}) {
    this.flashAny('success', text, options);
  }

  public flashDanger(text: string, options?: {}) {
    this.flashAny('danger', text, options);
  }

  public flashAny(level: string, text: string, options?: {}) {
    const params = { cssClass: `alert-${level}`, timeout: 5000, ...options };
    if (!this.flashMsg || !this.flashMsg['show']) {
      return;
    }

    this.flashMsg.show(text, params);
  }
}
