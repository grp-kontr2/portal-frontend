import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { CourseService } from './course.service';
import { Course, Project, Secret, User } from '../shared/models/models';
import { Error } from 'tslint/lib/error';

@Injectable()
export class UserService {


  constructor(private http: HttpClient, private courseService: CourseService) {
  }

  usersUrl = `${environment.baseUrl}/api/v1.0/users`;


  public getUser(id: string): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    console.log(`[USER] Find (${url})`);
    return this.http.get<User>(url);
  }

  public listUsers(): Observable<User[]> {
    const url = `${this.usersUrl}`;
    console.log(`[USER] List (${url})`);
    return this.http.get<User[]>(url);
  }

  public loadCoursesForUser(user: User): Observable<Course[]> {
    if (user.is_admin) {
      return this.courseService.listCourses();
    } else {
      const url = `${this.usersUrl}/${user.id}/courses`;
      console.log(`[USER] List courses (${url})`);
      return this.http.get<Course[]>(url);
    }
  }

  public createUser(username: string, name: string, email: string, uco: string, is_admin: boolean) {
    const url = `${this.usersUrl}`;
    const body = {
      'username': username,
      'name': name,
      'email': email,
      'uco': uco,
      'is_admin': is_admin
    };
    console.log(`[USER] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }

  public updateUser(user: User) {
    const url = `${this.usersUrl}/${user.id}`;
    const body = {
      username: user.username,
      email: user.email,
      uco: user.uco,
      name: user.name,
      is_admin: user.is_admin
    };
    console.log(`[USER] List courses (${url}): ${body}`);
    return this.http.put(url, body); // check body
  }

  public changeUserPassword(userId: string, oldPassword: string, newPassword: string) {
    const url = `${this.usersUrl}/${userId}/password`;
    const body = {
      old_password: oldPassword,
      new_password: newPassword
    };
    console.log(`[USER] Update password (${url})`);
    return this.http.put(url, body);
  }

  public deleteUser(userId: string) {
    const url = `${this.usersUrl}/${userId}`;
    console.log(`[USER] Delete (${url})`);
    return this.http.delete(url);
  }

  public loadSubmissionsForUser(userId: string, courseId: string, projectIds: string[]) {
    if ((courseId === null || courseId === '') && (projectIds.length > 0)) {
      console.warn('invalid param combination - course id missing');
      throw new Error('invalid param combination - course id missing');
    }
    let params = new HttpParams();
    params = params.append('course', courseId);
    projectIds.forEach(id => {
      params = params.append('project', id);
    });
    const url = `${this.usersUrl}/${userId}/submissions`;
    console.log(`[USER] List submissions (${url}): ${params}`);
    return this.http.get(url, { params: params });
  }

  // Move to courses
  public getUsersFiltered(courseId: string, roleIds: string[] = [], groupIds: string[]) {
    let params = new HttpParams();
    roleIds.forEach(id => {
      params = params.append('role', id);
    });
    groupIds.forEach(id => {
      params = params.append('group', id);
    });
    const url = `${environment.baseUrl}/api/v1.0/courses/${courseId}/users`;
    console.log(`[USER] List users filter (${url}): ${params}`);
    return this.http.get<User[]>(url, { params: params });
  }

  public listProjectsForUser(user: User) {
    const url = `${this.usersUrl}/${user.id}/projects`;
    console.log(`[USER] List projects (${url})`);
    return this.http.get<Project[]>(url);
  }
}

