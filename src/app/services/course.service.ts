import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Course, Group, Project, Role, User } from '../shared/models/models';


@Injectable()
export class CourseService {
  coursesUrl = `${environment.baseUrl}/api/v1.0/courses`;

  constructor(private http: HttpClient) {
  }

  public listCourses(): Observable<Course[]> { // will only work for sysadmin (see backend)
    const url = this.coursesUrl;
    console.log(`[COURSE] List (${url})`);
    return this.http.get<Course[]>(url);
  }

  public getCourse(id: string): Observable<Course> {
    const url = `${this.coursesUrl}/${id}`;
    console.log(`[COURSE] Find (${url})`);
    return this.http.get<Course>(url);
  }

  public loadProjectsForCourse(course: Course) {
    const url = `${this.coursesUrl}/${course.id}/projects`;
    console.log(`[COURSE] Find projects (${url})`);
    return this.http.get<Project[]>(url);
  }

  public loadRolesForCourse(course: Course) {
    const url = `${this.coursesUrl}/${course.id}/roles`;
    console.log(`[COURSE] Find roles (${url})`);
    return this.http.get<Role[]>(url);
  }

  public loadGroupsForCourse(course: Course) {
    const url = `${this.coursesUrl}/${course.id}/groups`;
    console.log(`[COURSE] Find groups (${url})`);
    return this.http.get<Group[]>(url);
  }

  public createCourse(name: string, codename: string, description: string) {
    const url = this.coursesUrl;
    const body = {
      'name': name,
      'codename': codename,
      'description': description,
    };
    console.log(`[COURSE] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }

  public updateCourse(course: Course) {
    const url = `${this.coursesUrl}/${course.id}`;
    const body = {
      'name': course.name,
      'codename': course.codename,
      'description': course.description,
    };
    console.log(`[COURSE] Update (${url}): ${body}`);
    return this.http.put(url, body);
  }

  public deleteCourse(id: string) {
    const url = `${this.coursesUrl}/${id}`;
    console.log(`[COURSE] Delete (${url})`);
    return this.http.delete(url);
  }

  public getUsersInCourseFiltered(course: Course, roleIds: string[], groupIds: string[]): Observable<User[]> {
    const params = new HttpParams();
    roleIds.forEach(id => {
      params.append('role', id);
    });
    groupIds.forEach(id => {
      params.append('group', id);
    });
    const url = `${this.coursesUrl}/${course.id}/users`;
    return this.http.get<User[]>(url, { params: params });
  }

  public doImport(target: string, source: string, group_users: boolean, role_users: boolean) {
    console.log(`[COURSE] import to ${target}`);
    return this.http.put(`${this.coursesUrl}/${target}/import`, {
      source_course: source,
      config: {
        roles: role_users ? 'with_users' : 'bare',
        groups: group_users ? 'with_users' : 'bare',
        projects: 'bare',
      }
    });
  }
}
