import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { Course, GitlabProject } from '../shared/models/models';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GitlabService {

  gitlabUrl = `${environment.baseUrl}/api/v1.0/gitlab`;

  constructor(private http: HttpClient) {}

  public listGitlabProjects(): Observable<GitlabProject[]> {
    const url = `${this.gitlabUrl}/projects`;
    console.log(`[GL] List (${url})`);
    return this.http.get<GitlabProject[]>(url, {withCredentials: true});
  }

  public getGitlabProject(name: string): Observable<GitlabProject> {
    const url = `${this.gitlabUrl}/project/?project=${name}`;
    console.log(`[GL] GET (${url})`);
    return this.http.get<GitlabProject>(url, {withCredentials: true});
  }

  public getServiceToken(): any { // will only work for sysadmin (see backend)
    const url = `${this.gitlabUrl}/service_token`;
    console.log(`[GL] Service token (${url}) - debug only mode`);
    return this.http.get(url, {withCredentials: true});
  }

  public enableGitlabProject(name: string): Observable<GitlabProject> {
    const url = `${this.gitlabUrl}/project/members?project=${name}`;
    console.log(`[GL] ENABLE (${url})`);
    return this.http.post<GitlabProject>(url, {}, {withCredentials: true});
  }

  public listGitlabProjectsMembers(name: string): Observable<GitlabProject> {
    const url = `${this.gitlabUrl}/project/members?project=${name}`;
    console.log(`[GL] GET (${url})`);
    return this.http.get<GitlabProject>(url, {withCredentials: true});
  }
}
