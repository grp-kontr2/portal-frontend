import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client, Permissions, Role, User } from '../shared/models/models';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class RoleService {
  constructor(private http: HttpClient) {
  }

  coursesUrl = `${environment.baseUrl}/api/v1.0/courses`;

  public findRoles(courseId: string): Observable<Role[]> {
    const url = `${this.coursesUrl}/${courseId}/roles`;
    console.log(`[ROLE] Find (${url})`);
    return this.http.get<Role[]>(url);
  }

  public findRole(courseId: string, roleId: string): Observable<Role> {
    const url = `${this.coursesUrl}/${courseId}/roles/${roleId}`;
    console.log(`[ROLE] Find (${url})`);
    return this.http.get<Role>(url);
  }

  public createRole(courseId: string, name: string, codename: string, description: string) {
    const body = {
      name: name,
      codename: codename,
      description: description,
    };

    const url = `${this.coursesUrl}/${courseId}/roles`;
    console.log(`[ROLE] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }


  public updateRole(role: Role) {
    const body = {
      name: role.name,
      description: role.description,
      codename: role.codename
    };
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}`;
    console.log(`[ROLE] Update (${url}): ${body}`);
    return this.http.put(url, body);
  }

  public deleteRole(courseId: string, roleId: string) {
    const url = `${this.coursesUrl}/${courseId}/roles/${roleId}`;
    console.log(`[ROLE] Delete (${url})`);
    return this.http.delete(url);
  }

  public updateRolePermissions(role: Role, permissions: Permissions) {
    const body = permissions;
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}/permissions`;
    return this.http.put(url, body);
  }

  public getRolePermissions(role: Role): Observable<Permissions> {
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}/permissions`;
    return this.http.get<Permissions>(url);
  }

  public removeUser(role: Role, userId: string) {
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}/clients/${userId}`;
    return this.http.delete(url);
  }

  public addUser(role: Role, userId: string) {
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}/clients/${userId}`;
    return this.http.put(url, {});
  }

  getClients(role: Role) {
    const url = `${this.coursesUrl}/${role.course.id}/roles/${role.id}/clients`;
    return this.http.get<Client[]>(url);
  }
}
