import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Client } from '../shared/models/models';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  clients = `${environment.baseUrl}/api/v1.0/clients`;

  public getClient(id: string): Observable<Client> {
    const url = `${this.clients}/${id}`;
    console.log(`[CLIENT] Find (${url})`);
    return this.http.get<Client>(url);
  }
}
