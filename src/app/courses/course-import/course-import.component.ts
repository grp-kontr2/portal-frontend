import { Component, OnDestroy, OnInit } from '@angular/core';
import { Course, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CourseService } from '../../services/course.service';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { PermissionsService } from '../../services/permissions.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-course-import',
  templateUrl: './course-import.component.html',
  styleUrls: [ './course-import.component.scss' ]
})
export class CourseImportComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  subscriptions: Subscription[] = [];
  data: FormGroup;
  sourceTargetDistinctVar: boolean;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private auth: AuthService,
              private courseService: CourseService,
              private userService: UserService,
              private flash: FlashService,
              private router: Router,
              private permissions: PermissionsService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      this.checkCourses(this.loggedInUser);
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.data = this.fb.group({
      source_c: [ null, Validators.required ],
      target_c: [ null, Validators.required ],
      users_groups: [ false ],
      users_roles: [ false ]
    });
  }

  checkCourses(user: User) {
    if (user.courses == null) {
      console.log(`user\'s courses are ${user.courses}, loading`);
      this.loadCourses(user);
    }
  }

  loadCourses(user: User) {
    if (user.is_admin) {
      console.log('user is admin -> loading');
      this.subscriptions.push(this.courseService.listCourses().subscribe(res => {
        user.courses = res;
        this.flash.loadResources(res, 'courses');
      }, err => this.flash.handleError(err)));
    } else {
      console.log('user is not admin -> loading');
      this.subscriptions.push(this.userService.loadCoursesForUser(user).subscribe(res => {
        user.courses = res;
        console.log('set user.courses to ', user.courses);
        this.auth.setLoggedInUser(user);
        this.flash.loadResources(res, 'courses');
      }, err => this.flash.handleError(err)));
      console.log('loaded user course list');
    }
  }

  resetForm() {
    this.data.reset();
  }

  doImport() {
    if (this.data.status !== 'VALID') {
      console.warn('[COURSE] Invalid import form submitted', this.data);
      return;
    }

    const values = this.data.value;
    const source = values[ 'source_c' ];
    const target = values[ 'target_c' ];

    if (!this.loggedInUser.is_admin) {
      if (!this.permissions.checkUpdateCourse(this.loggedInUser, target.id)) {
        this.flash.flashDanger(`Not permitted to import to course ${target.id}`);
        return;
      }

      if (!this.permissions.checkAll(this.loggedInUser, source.id, [ 'view_course_full' ])) {
        this.flash.flashDanger(`Not permitted to import to course ${target.id}`);
        return;
      }
    }
    const users_groups = values [ 'users_groups' ];
    const users_roles = values [ 'users_roles' ];

    this.subscriptions.push(this.courseService.doImport(target.id, source.id, users_groups, users_roles).subscribe(
      () => {
        this.router.navigateByUrl(`/courses/${target.id}`).then(
          () => {
            this.flash.flashSuccess('Import successful.');
          }
        );
      }
    ));
  }

  get source_c() {
    return this.data.get('course_c');
  }

  get target_c() {
    return this.data.get('target_c');
  }

  sourceTargetDistinct(): boolean {
    const res = !(this.data.value[ 'source_c' ].id === this.data.value[ 'target_c' ].id);
    console.log('checking distinct: ', res);
    this.sourceTargetDistinctVar = res;
    return res;
  }
}
