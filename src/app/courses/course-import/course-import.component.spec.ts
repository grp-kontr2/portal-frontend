import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseImportComponent } from './course-import.component';

describe('CourseImportComponent', () => {
  let component: CourseImportComponent;
  let fixture: ComponentFixture<CourseImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
