import { Component, OnDestroy, OnInit } from '@angular/core';
import { Course, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: [ './role-create.component.scss' ]
})
export class RoleCreateComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  loggedInUser: User;
  course: Course;
  createData: FormGroup;
  subscriptions: Subscription[] = [];

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      // route guard based on user's permissions not usable here, see
      // https://github.com/angular/angular/issues/20805
      if (!this.userCanCreateRole()) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flash.flashDanger('You are not authorized to create new roles.');
          }
        );
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    interface Params {
      required?: boolean;
      value?: any;
    }

    const empty = ({ required = false, value = '' }: Params) => {
      const req = required ? Validators.required : null;
      return this.fb.control({ value: value }, req);
    };

    this.createData = this.fb.group({
      name: empty({ required: true }),
      codename: empty({ required: true }),
      description: empty({ required: false }),
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.warn('invalid form submitted');
      return;
    }
    console.log('Project create form data: ', data);
    const createRoleSubscription = this.service.createRole(this.course.id, data[ 'name' ], data[ 'codename' ], data[ 'description' ])
      .subscribe(
        () => {
          this.router.navigateByUrl(`/courses/${this.course.id}/roles`).then(() => {
            this.flash.flashSuccess(`Created role ${data[ 'codename' ]}.`);
          });
        }, (error: any) => {
          this.flash.flashDanger(`Error creating role ${data[ 'codename' ]}.`);
          this.flash.handleError(error);
        });
    this.subscriptions.push(createRoleSubscription);
  }

  userCanCreateRole() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
