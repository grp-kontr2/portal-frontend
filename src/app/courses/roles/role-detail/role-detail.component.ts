import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project, Role, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: [ './role-detail.component.scss' ]
})
export class RoleDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  role: Role;
  formData: FormGroup;
  showConfigAtLoad: boolean;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, role: Role, showConfigAtLoad: boolean }) => {
      this.loggedInUser = data.loggedInUser;
      this.role = data.role;
      this.createForm();
      this.showConfigAtLoad = data.showConfigAtLoad ? data.showConfigAtLoad : false;
      this.initFormValues();
    }));
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('initializing form values');
    if (this.role == null) {
      console.log('role was == null');
      return;
    }
    console.log('setting values for role detail form');
    this.formData.setValue({
      name: this.role.name,
      codename: this.role.codename,
      description: this.role.description,
    });
  }

  createForm() {
    interface Params {
      required?: boolean;
      value?: any;
    }

    const empty = ({ required = false, value = '' }: Params) => {
      const req = required ? Validators.required : null;
      return this.fb.control({ value: value, disabled: !this.userCanUpdateRole() }, req);
    };

    this.formData = this.fb.group({
      name: empty({required: true}),
      codename:  empty({required: true}),
      description: empty({required: false}),
    });
  }

  updateRole() {
    console.log('[FORM] updating Form Group');
    if (!this.formData.valid) {
      console.warn('[FORM] Invalid form submitted');
      return;
    }
    const newValues = this.extractRoleFromFormValues();
    const updateRoleSubscription = this.service.updateRole(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess('Update successful.');
        });
      }, (error: any) => this.flash.handleError(error));
    this.subscriptions.push(updateRoleSubscription);
  }

  private extractRoleFromFormValues() {
    const data = this.formData.value;
    const newValues = {
      id: this.role.id,
      course: this.role.course,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
    } as Role;
    console.log('[ROLE] Extracted role: ', newValues);
    return newValues;
  }

  deleteRole() {
    const deleteRoleSubscription = this.service.deleteRole(this.role.course.id, this.role.id).subscribe(
      () => {
        console.log('[FORM] Deleted group ', this.role.id);
        this.router.navigateByUrl(`/courses/${this.role.course.id}/roles`).then(() => {
          this.flash.flashSuccess(`Deleted role ${this.role.id}`);
        });

      }, (error: any) => this.flash.handleError(error.error));
    this.subscriptions.push(deleteRoleSubscription);
  }

  resetForm() {
    this.formData.reset();
    this.initFormValues();
  }

  userCanUpdateRole() {
    return this.permissions.checkWriteRoles(this.loggedInUser, this.role.course);
  }

  userCanViewPermissions() {
    return this.userCanUpdateRole() || this.permissions.checkViewCourseFull(this.loggedInUser, this.role.course);
  }
}
