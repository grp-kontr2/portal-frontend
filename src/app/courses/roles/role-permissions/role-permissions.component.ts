import { Component, Input, OnInit } from '@angular/core';
import { Permissions, Role, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-role-permissions',
  templateUrl: './role-permissions.component.html',
  styleUrls: [ './role-permissions.component.scss' ]
})
export class RolePermissionsComponent implements OnInit {

  constructor(private fb: FormBuilder,
              public service: RoleService,
              private flash: FlashService,
              private router: Router,
              private route: ActivatedRoute,
              private permissions: PermissionsService
  ) {
  }

  @Input() loggedInUser: User;
  @Input() role: Role;
  permissionsData: FormGroup;
  permissionsDict = {};

  ngOnInit() {
    console.log('Received role: ', this.role);
    console.log('Received user: ', this.loggedInUser);
    this.createForm();
    this.initFormValues();
  }

  fillPermDict() {
    const permissions = (this.role) ? this.role : new Permissions();
    Object.entries(permissions).forEach(([ k, v ]) => {
      if (typeof v === 'boolean') {
        this.permissionsDict[ k ] = v;
      }
    });
    console.log('[FILL] Permissions dict: ', this.permissionsDict);
    return this.permissionsDict;
  }

  permissionsEntries() {
    return Object.entries(this.permissionsDict);
  }

  createForm() {
    this.fillPermDict();
    const params = {};
    Object.keys(this.permissionsDict).forEach((key) => {
      const control = {value: this.permissionsDict[key], disabled: this.userCanUpdatePermissions()};
      params[key] = this.fb.control(control);
    });
    this.permissionsData = this.fb.group(params);
  }

  initFormValues() {
    const values = this.fillPermDict();
    this.permissionsData.setValue(values);
  }

  resetForm() {
    this.permissionsData.reset();
    this.initFormValues();
  }

  updatePermissions() {
    console.log('[FORM] updating roles permissions');
    if (this.permissionsData.status !== 'VALID') {
      console.warn(`[FORM] invalid form submitted (${this.permissionsData.status})`,
        this.permissionsData.getRawValue());
      return;
    }
    const data = this.permissionsData.value;
    const newPermissions = data as Permissions;
    console.log('[FORM] Update permissions: ', newPermissions);
    this.service.updateRolePermissions(this.role, newPermissions).subscribe(() => {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
        this.flash.flashSuccess('Permissions update successful.');
      });
    }, (error: any) => {
      this.flash.handleError(error);
    });
  }

  userCanViewPermissions() {
    return this.permissions.checkAll(this.loggedInUser, this.role.course, [ 'read_roles' ]);
  }

  userCanUpdatePermissions() {
    return this.permissions.checkWriteRoles(this.loggedInUser, this.role.course);
  }

}
