import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Client, Course, Role, User } from '../../../shared/models/models';

import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RoleService } from '../../../services/role.service';
import { PermissionsService } from '../../../services/permissions.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-role-users',
  templateUrl: './role-users.component.html',
  styleUrls: [ './role-users.component.scss' ]
})
export class RoleUsersComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  role: Role;
  course: Course;
  subscriptions: Subscription[] = [];
  formData: FormGroup;
  clients: Client[];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              public flash: FlashService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    const subscription = this.route.data.subscribe((data: { loggedInUser: User, role: Role, course: Course }) => {
      if (!this.userCanAddUser(data.loggedInUser, data.course)) {

        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flash.flashDanger('You are not authorized to modify users in the list.');
          }
        );
      }
      this.loggedInUser = data.loggedInUser as User;
      this.course = data.course;
      this.updateEntity(data.role);
    });
    this.subscriptions.push(subscription);
  }

  updateEntity(role: Role) {
    this.role = role;
    this.clients = this.role.clients;
    this.reloadClients();
  }

  private reloadClients() {
    this.loadingIndicator = true;
    const roleUsersSubs = this.service.getClients(this.role).subscribe((clients) => {
      this.clients = clients;
      this.loadingIndicator = false;
      this.flash.loadResources(clients, 'Role clients');
    }, (err) => this.flash.handleError(err));

    this.subscriptions.push(roleUsersSubs);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doRemoveUser(userId: string) {
    const removeUserSub = this.service.removeUser(this.role, userId).subscribe(() => {
      this.reloadClients();
      this.flash.flashSuccess(`Updated users list.`);
    }, (err: any) => {
      this.flash.flashDanger(`Error removing user: ${userId}.`, { cssClass: 'alert-danger', timeout: 10000 });
      this.flash.handleError(err);
    });
    this.subscriptions.push(removeUserSub);
  }

  createForm() {
    this.formData = this.fb.group({
      username: [ '' ],
    });
  }

  userCanAddUser(user?: User, course?: Course) {
    user = user || this.loggedInUser;
    course = course || this.course;
    if (user === undefined || course === undefined) {
      return false;
    }
    return this.permissions.checkWriteRoles(user, course);
  }

  doAddUser() {
    const data = this.formData.value;
    if (this.formData.status !== 'VALID') {
      console.warn('invalid form submitted');
      return;
    }
    const addUserSub = this.service.addUser(this.role, data[ 'username' ]).subscribe(() => {
      this.reloadClients();
      this.flash.flashSuccess(`Updated users list.`);
    }, (err: any) => {
      this.flash.flashDanger(`Error adding user: ${data[ 'username' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
      this.flash.handleError(err);
    });
    this.subscriptions.push(addUserSub);
  }
}
