import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Role, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: [ './role-list.component.scss' ]
})
export class RoleListComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  course: Course;
  roles: Role[];
  temp: Role[];
  subscriptions: Subscription[] = [];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public roleService: RoleService,
              public flash: FlashService,
              private permissions: PermissionsService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.roles = this.course.roles;
      this.temp = [];
      this.loadRoles();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  userCanCreateRole() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }

  loadRoles() {
    const listSubscription = this.roleService.findRoles(this.course.id);
    this.loadingIndicator = true;
    this.subscriptions.push(
      listSubscription.subscribe((roles) => {
        this.course.roles = roles;
        this.roles = roles;
        this.temp = [ ...roles ];
        console.log('set course.roles to ', roles);
        this.flash.loadResources(roles, 'roles');
        this.loadingIndicator = false;
      }, err => { this.flash.handleError(err); })
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.roles = this.temp.filter(function (d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  userCanUpdateUsers() {
    return this.permissions.checkWriteRoles(this.loggedInUser, this.course);
  }
}
