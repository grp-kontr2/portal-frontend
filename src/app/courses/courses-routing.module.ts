import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { CourseCreateComponent } from './course-create/course-create.component';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';
import { CourseResolver } from './course-resolver.service';
import { AdminGuard } from '../guards/admin.guard';
import {CourseImportComponent} from './course-import/course-import.component';

const routes: Routes = [
  {
    path: '',
    component: CourseListComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: 'create',
    component: CourseCreateComponent,
    canActivate: [ AdminGuard ]
  },
  {
    path: 'import',
    pathMatch: 'full',
    runGuardsAndResolvers: 'always',
    component: CourseImportComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
    }
  },
  {
    path: ':cid',
    pathMatch: 'full',
    runGuardsAndResolvers: 'always',
    component: CourseDetailComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver
    }
  },
  {
    path: ':cid/projects',
    loadChildren: 'app/courses/projects/projects.module#ProjectsModule',
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver
    },
  },
  {
    path: ':cid/groups',
    loadChildren: 'app/courses/groups/groups.module#GroupsModule',
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver
    }
  },
  {
    path: ':cid/roles',
    loadChildren: 'app/courses/roles/roles.module#RolesModule',
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CoursesRoutingModule {
}
