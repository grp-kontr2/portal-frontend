import { Injectable } from '@angular/core';
import { Course } from '../shared/models/models';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CourseService } from '../services/course.service';
import { map, take } from 'rxjs/operators';
import { FlashService } from '../services/flash.service';


@Injectable()
export class CourseResolver implements Resolve<Course> {

  constructor(public service: CourseService,
              private router: Router,
              private flash: FlashService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Course> {
    const cid = route.paramMap.get('cid');

    return this.service.getCourse(cid).pipe(take(1), map(course => {
      if (course) {
        return course;
      } else {
        this.router.navigateByUrl('/courses').then(
          () => {
            this.flash.flashDanger(`Course ${cid} not found.`);
          }, (error: any) => {
            console.error('[RESOLVE] Could not navigate to course list: ', error.error);
            this.flash.handleError(error);
          });
        return null;
      }
    }));
  }
}
