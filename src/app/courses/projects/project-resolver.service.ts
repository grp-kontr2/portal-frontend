import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Project } from '../../shared/models/models';
import { Observable } from 'rxjs';
import { ProjectService } from '../../services/project.service';
import { map, take } from 'rxjs/operators';
import { FlashService } from '../../services/flash.service';


@Injectable()
export class ProjectResolver implements Resolve<Project> {

  constructor(public service: ProjectService, private router: Router, private flash: FlashService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> {
    console.log('route data in group resolver: ', route.data);
    const course = route.parent.data[ 'course' ];
    const pid = route.paramMap.get('pid');

    return this.service.findProject(course.id, pid)
      .pipe(take(1), map(project => {
          if (project) {
            return project;
          }
          this.router.navigateByUrl(`/courses/${course.id}`).then(
            () => {
              this.flash.flashDanger(`Project ${pid} not found in course ${course.id}.`);
            }, (error: any) => {
              console.error(`Could not navigate to course ${course.id}: `, error.error);
              this.flash.handleError(error);
            });
          return null;

        })
      );
  }

}
