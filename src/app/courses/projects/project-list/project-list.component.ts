import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Project, User } from '../../../shared/models/models';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../../services/permissions.service';
import { ProjectService } from '../../../services/project.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: [ './project-list.component.scss' ]
})
export class ProjectListComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  course: Course;
  projects: Project[];
  temp: Project[];
  subscriptions: Subscription[] = [];


  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              private permissions: PermissionsService,
              private flash: FlashService,
              private projectService: ProjectService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.projects = data.course.projects;
      this.temp = [];
      this.loadProjects();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  userCanCreateProject() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }

  loadProjects() {
    const listSubscription = this.projectService.findProjects(this.course.id);
    this.loadingIndicator = true;
    this.subscriptions.push(
      listSubscription.subscribe((projects) => {
        this.course.projects = projects;
        this.projects = projects;
        this.temp = [ ...projects ];
        console.log('set course.projects to ', projects);
        this.loadingIndicator = false;
        this.flash.loadResources(projects, 'projects');
      }, err => { this.flash.handleError(err); })
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.projects = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  updateTestFiles(projectId: string) {
    this.subscriptions.push(this.projectService.updateProjectFiles(this.course.id, projectId).subscribe(() => {}));
  }

  userCanUpdateProjectFiles() {
    return this.permissions.checkWriteProjects(this.loggedInUser, this.course);
  }
}

