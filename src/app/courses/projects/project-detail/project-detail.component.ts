import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { ProjectService } from '../../../services/project.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../../services/permissions.service';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: [ './project-detail.component.scss' ]
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  project: Project;
  formData: FormGroup;
  showConfig: boolean;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public projectService: ProjectService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
  }

  get course() {
    if (this.project == null) {
      return null;
    }

    return this.project.course;
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, project: Project, showConfigAtLoad: boolean }) => {
      this.loggedInUser = data.loggedInUser;
      this.project = data.project;
      this.showConfig = data.showConfigAtLoad ? data.showConfigAtLoad : false;
      this.createForm();
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('initializing form values');
    if (this.project == null) {
      console.log('project was == null');
      return;
    }
    this.formData.setValue({
      name: this.project.name,
      description: this.project.description,
      codename: this.project.codename,
      assignment_url: this.project.assignment_url,
      submit_configurable: this.project.submit_configurable,
      submit_instructions: this.project.submit_instructions,
    });
  }

  createForm() {
    interface Params {
      required?: boolean;
      value?: any;
    }

    const empty = ({ required = false, value = '' }: Params) => {
      const req = required ? Validators.required : null;
      return this.fb.control({ value: value, disabled: !this.userCanUpdateProject() }, req);
    };
    this.formData = this.fb.group({
      name: empty({ required: true }),
      codename: empty({ required: true }),
      description: empty({ required: true }),
      assignment_url: empty({ required: true }),
      submit_configurable: empty({ value: false }),
      submit_instructions: empty({}),
    });
  }

  updateProject() {
    console.log('updating project');
    if (this.formData.status !== 'VALID') {
      this.flash.flashDanger('Invalid form submitted');
      return;
    }
    const data = this.formData.value;
    const newValues = {
      id: this.project.id,
      course: this.project.course,
      ...data
    } as Project;
    this.subscriptions.push(this.projectService.updateProject(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess('Update successful.');
        });
      }, (error: any) => {
        this.flash.handleError(error);
      }
    ));
  }

  deleteProject() {
    const projectId = this.project.id;
    const deleteProjectSubscription = this.projectService.deleteProject(this.course.id, projectId).subscribe(
      () => {
        console.log('Deleted group ', projectId);
        this.router.navigateByUrl(`/courses/${this.course.id}/projects`).then(() => {
          this.flash.flashSuccess(`Deleted project ${projectId}`);
        });

      }, (error: any) => {
        this.flash.handleError(error);
      });
    this.subscriptions.push(deleteProjectSubscription);
  }

  resetForm() {
    this.formData.reset();
    this.initFormValues();
  }

  userCanUpdateProject() {
    if (this.course == null) {
      return true;
    }

    return this.permissions.checkWriteProjects(this.loggedInUser, this.course);
  }

  userCanCreateSubmissions() {
    return this.permissions.checkAll(this.loggedInUser, this.course, [ 'create_submissions' ]);
  }

  updateTestFiles() {
    this.subscriptions.push(this.projectService.updateProjectFiles(this.course.id, this.project.id).subscribe(() => {
    }));
  }
}
