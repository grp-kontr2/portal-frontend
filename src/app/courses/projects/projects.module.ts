import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectConfigComponent } from './project-config/project-config.component';
import {ProjectResolver} from './project-resolver.service';
import {ShowConfigAtLoadResolver} from './show-config-at-load-resolver.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  providers: [
    ProjectResolver,
    ShowConfigAtLoadResolver
  ],
  declarations: [
    ProjectCreateComponent,
    ProjectDetailComponent,
    ProjectListComponent,
    ProjectConfigComponent
  ]
})
export class ProjectsModule { }
