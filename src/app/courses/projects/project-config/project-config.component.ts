import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Course, Project, ProjectConfig, User } from '../../../shared/models/models';
import { ProjectService } from '../../../services/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { DatePipe } from '@angular/common';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-project-config',
  templateUrl: './project-config.component.html',
  styleUrls: [ './project-config.component.scss' ]
})
export class ProjectConfigComponent implements OnInit {
  @Input() loggedInUser: User;
  @Input() project: Project;
  configData: FormGroup;
  allowedFromDate: Date;

  get course(): Course {
    return this.project.course;
  }

  constructor(private fb: FormBuilder,
              public projectService: ProjectService,
              private flash: FlashService,
              private router: Router,
              private route: ActivatedRoute,
              private permissions: PermissionsService,
              private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.initFormValues();
  }

  createForm() {
    const empty = (value: any = '') => {
      return this.fb.control({ value: value, disabled: !this.userCanUpdateProject() });
    };
    this.configData = this.fb.group({
      testFilesSource: empty(),
      testFilesSubdir: empty(),
      fileWhitelist: empty(),
      submissionParameters: empty(),
      submissionsAllowedFrom: empty(new Date()),
      submissionsAllowedTo: empty(new Date()),
      archiveFrom: empty(new Date()),
    });
  }

  initFormValues() {
    // TODO: revamp date -> date, time, validate input
    console.log('[Project] Project config: ', this.project.config);
    const to_date = (date) => {
      return (date == null) ? new Date() : new Date(date);
    };

    this.configData.patchValue({
      testFilesSource: this.project.config.test_files_source,
      testFilesSubdir: this.project.config.test_files_subdir,
      fileWhitelist: this.project.config.file_whitelist,
      submissionParameters: this.project.config.submission_parameters,
      submissionsAllowedFrom: to_date(this.project.config.submissions_allowed_from),
      submissionsAllowedTo: to_date(this.project.config.submissions_allowed_to),
      archiveFrom: to_date(this.project.config.archive_from),
    });
  }

  updateConfig() {
    console.log('updating group config');
    if (this.configData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.configData.value;
    const newValues = {
      project: this.project,
      test_files_source: data[ 'testFilesSource' ],
      test_files_subdir: data[ 'testFilesSubdir' ],
      file_whitelist: data[ 'fileWhitelist' ],
      submission_parameters: data[ 'submissionParameters' ],
      submissions_allowed_from: data[ 'submissionsAllowedFrom' ].toISOString(),
      submissions_allowed_to: data[ 'submissionsAllowedTo' ].toISOString(),
      archive_from: data[ 'archiveFrom' ].toISOString()
    } as ProjectConfig;
    this.projectService.updateProjectConfig(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route, preserveQueryParams: true }).then(() => {
          this.flash.flashSuccess('Configuration update successful.');
        });
      }, (error: any) => {
        this.flash.flashDanger(`Configuration update unsuccessful: ${error.error}`);
        this.flash.handleError(error);
      }
    );
  }

  resetForm() {
    this.configData.reset();
    this.initFormValues();
  }

  userCanViewFullConfig() {
    return this.permissions.checkReadProjects(this.loggedInUser, this.course);
  }

  userCanUpdateProject() {
    if (!this.project) {
      return true;
    }

    return this.permissions.checkWriteProjects(this.loggedInUser, this.project.course);
  }
}
