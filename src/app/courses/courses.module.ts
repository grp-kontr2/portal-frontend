import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CoursesRoutingModule } from './courses-routing.module';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { SharedModule } from '../shared/shared.module';
import { CourseCreateComponent } from './course-create/course-create.component';
import { CourseResolver } from './course-resolver.service';
import { CourseImportComponent } from './course-import/course-import.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CourseResourceComponent } from './course-detail/course-resource.component';


@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    CourseResolver
  ],
  declarations: [
    CourseListComponent,
    CourseResourceComponent,
    CourseDetailComponent,
    CourseCreateComponent,
    CourseImportComponent
  ]
})
export class CoursesModule {
}
