import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PermissionsService } from '../../../services/permissions.service';
import { GroupService } from '../../../services/group.service';
import { Subscription } from 'rxjs';
import { Course, User } from '../../../shared/models/models';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: [ './group-create.component.scss' ]
})
export class GroupCreateComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: GroupService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
  }

  loggedInUser: User;
  course: Course;
  createData: FormGroup;
  subscriptions: Subscription[] = [];


  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.createForm();
      // route guard based on user's permissions not usable here, see
      // https://github.com/angular/angular/issues/20805
      if (!this.userCanCreateGroup()) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flash.flashDanger('You are not authorized to create new groups.');
          }
        );
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    interface Params {
      required?: boolean;
      value?: any;
    }

    const empty = ({ required = false, value = '' }: Params) => {
      const req = required ? Validators.required : null;
      return this.fb.control({ value: value, disabled: false }, req);
    };

    this.createData = this.fb.group({
      name: empty({required: true}),
      codename:  empty({required: true}),
      description: empty({required: false}),
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.warn('[FORM] invalid form submitted', this.createData);
      this.flash.flashDanger(`Invalid form submitted`);
      return;
    }
    console.log('[FORM] Project create form data: ', data);
    const createRoleSubscription = this.service.createGroup(this.course.id, data[ 'name' ], data[ 'description' ], data[ 'codename' ])
      .subscribe(
      () => {
        this.router.navigateByUrl(`/courses/${this.course.id}/groups`).then(() => {
          this.flash.flashSuccess(`Created group ${data[ 'name' ]}.`);
        });
      }, (error: any) => {
        this.flash.flashDanger(`Error creating group ${data[ 'name' ]}.`);
          this.flash.handleError(error);
      });
    this.subscriptions.push(createRoleSubscription);
  }


  userCanCreateGroup() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
