import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Group, Role, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../../services/permissions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { GroupService } from '../../../services/group.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-group-users',
  templateUrl: './group-users.component.html',
  styleUrls: [ './group-users.component.scss' ]
})
export class GroupUsersComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  group: Group;
  subscriptions: Subscription[] = [];
  formData: FormGroup;
  users: User[];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: GroupService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    this.loadingIndicator = false;
    const subscription = this.route.data.subscribe((data: { loggedInUser: User, group: Group }) => {
      if (!this.userCanManageUsers(data.loggedInUser, data.group.course)) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flash.flashDanger('You are not authorized to modify users in the list.');
          }
        );
      }
      this.loggedInUser = data.loggedInUser;
      this.group = data.group;
      this.loadingIndicator = false;
    });
    this.subscriptions.push(subscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  userCanManageUsers(user?: User, course?: Course) {
    user = user || this.loggedInUser;
    course = course || this.group.course;
    if (!user || !course) {
      return false;
    }
    return this.permissions.checkWriteGroups(user, course);
  }

  createForm() {
    this.formData = this.fb.group({
      username: [ '' ],
    });
  }

  doRemoveUser(userId: string) {
    const removeUserSub = this.service.removeUser(this.group, userId).subscribe(() => {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
        this.flash.flashSuccess(`Updated users list.`);
      }, (error: any) => {
        this.flash.flashDanger(`Error removing user: ${userId}.`);
        this.flash.handleError(error);
      });
    });
    this.subscriptions.push(removeUserSub);
  }

  doAddUser() {
    const data = this.formData.value;
    if (this.formData.status !== 'VALID') {
      console.warn('invalid form submitted', this.formData);
      this.flash.flashDanger('Invalid form submitted.');
      return;
    }
    const addUserSub = this.service.addUser(this.group, data[ 'username' ]).subscribe(() => {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
        this.flash.flashSuccess(`Updated users list.`);
      }, (error: any) => {
        this.flash.flashDanger(`Error adding user: ${data[ 'username' ]}.`);
        this.flash.handleError(error);
      });
    });
    this.subscriptions.push(addUserSub);
  }
}
