import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Group } from '../../shared/models/models';
import { Observable } from 'rxjs';
import { GroupService } from '../../services/group.service';
import { map, take } from 'rxjs/operators';
import { FlashService } from '../../services/flash.service';

@Injectable()
export class GroupResolver implements Resolve<Group> {

  constructor(public service: GroupService,
              private router: Router,
              private flash: FlashService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Group> | Promise<Group> | Group {
    console.log('[RESOLVE] route data in group resolver: ', route.data);
    const course = route.parent.data[ 'course' ];
    const gid = route.paramMap.get('gid');
    return this.service.findGroup(course.id, gid).pipe(take(1), map(role => {
      if (role) {
        return role;
      }
      this.router.navigateByUrl(`/courses/${course.id}`).then(
        () => {
          this.flash.flashDanger(`Group ${gid} not found in course ${course.id}.`);
        }, (error: any) => {
          console.error(`[RESOLVE] Could not navigate to course ${course.id}: `, error.error);
          this.flash.handleError(error);
        });
      return null;
    }));
  }
}
