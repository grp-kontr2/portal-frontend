import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PermissionsService } from '../../../services/permissions.service';
import { GroupService } from '../../../services/group.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Course, Group, Project, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';

@Component({
  selector: 'app-group-projects',
  templateUrl: './group-projects.component.html',
  styleUrls: [ './group-projects.component.scss' ]
})
export class GroupProjectsComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  group: Group;
  course: Course;
  subscriptions: Subscription[] = [];
  formData: FormGroup;
  projects: Project[];
  temp: Project[];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: GroupService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  userCanManageProjects(user?: User, course?: Course) {
    user = user || this.loggedInUser;
    course = course || this.course;
    return this.permissions.checkWriteGroups(user, course);
  }

  ngOnInit() {
    this.loadingIndicator = true;
    const subscription = this.route.data.subscribe(
      (data: { loggedInUser: User, group: Group, course: Course }) => {
        this.loggedInUser = data.loggedInUser;
        this.group = data.group;
        this.course = data.course;
        this.projects = data.course.projects;
        this.temp = [ ...data.course.projects ];
        this.loadingIndicator = false;
        this.flash.loadResources(data.course.projects, 'projects');
        if (!this.userCanManageProjects()) {
          this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
              this.flash.flashDanger('You are not authorized to modify projects in the list.');
            }
          );
        }
      });
    this.subscriptions.push(subscription);
  }

  isActive(project: Project): boolean {
    return this.group.projects.some((p: Project) => p.id === project.id);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.formData = this.fb.group({
      project_name: [ '' ],
    });
  }

  doAction(project: Project) {
    if (this.isActive(project)) {
      this.removeProject(project);
    } else {
      this.addProject(project);
    }
  }

  enableAllProjects() {
    this.projects.forEach((project) => {
      if (!this.isActive(project)) {
        this.addProject(project);
      }
    });
  }


  disableAllProjects() {
    this.projects.forEach((project) => {
      if (this.isActive(project)) {
        this.removeProject(project);
      }
    });
  }

  private removeProject(project: Project) {
    const removeProjectSub = this.service.removeProject(this.group, project.id)
      .subscribe(() => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess(`Updated projects list.`);
        }, (error: any) => {
          console.error('Error removing project ', error);
          this.flash.flashDanger(`Error removing project: ${project.id}.`);
          this.flash.handleError(error);
        });
      });
    this.subscriptions.push(removeProjectSub);
  }

  private addProject(project: Project) {
    const addProjectSub = this.service.addProject(this.group, project.id)
      .subscribe(() => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess(`Updated projects list.`);
        }, (error: any) => {
          console.error('Error adding user ', error);
          this.flash.flashDanger(`Error adding project: ${project.id}.`);
          this.flash.handleError(error);
        });
      });
    this.subscriptions.push(addProjectSub);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.projects = this.temp.filter(function (d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
