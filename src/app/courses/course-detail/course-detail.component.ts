import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../../services/course.service';
import { Course, User } from '../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../services/permissions.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: [ './course-detail.component.scss' ]
})
export class CourseDetailComponent implements OnInit, OnDestroy {
  course: Course;
  loggedInUser: User;
  updateFormData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private service: CourseService,
              private fb: FormBuilder,
              private flash: FlashService,
              public auth: AuthService,
              private router: Router,
              public permissions: PermissionsService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.createForm();
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  initFormValues() {
    console.log('setting values for course detail form');
    console.log('have course: ', this.course);
    this.updateFormData.setValue({
      name: this.course.name,
      codename: this.course.codename,
      description: this.course.description,
    });
  }

  createForm() {
    this.updateFormData = this.fb.group({
      name: [ { value: '', disabled: !this.userCanUpdateCourse() }, Validators.required ],
      codename: [ { value: '', disabled: !this.userCanUpdateCourse() }, Validators.required ],
      description: [ { value: '', disabled: !this.userCanUpdateCourse() } ],
    });
  }

  updateCourse() {
    if (this.updateFormData.status !== 'VALID') {
      console.warn('[COURSE] Invalid form submitted', this.updateFormData);
      return;
    }
    const data = this.updateFormData.value;
    const newValues = {
      id: this.course.id,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
    } as Course;
    const updateCourseSubscription = this.service.updateCourse(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess('Update successful.');
        });
      }, (error: any) => this.flash.handleError(error));
    this.subscriptions.push(updateCourseSubscription);
  }

  deleteCourse() {
    const courseId = this.course.id;
    this.subscriptions.push(this.service.deleteCourse(courseId).subscribe(
      () => {
        console.log('Deleted course ', courseId);
        this.router.navigateByUrl('/courses').then(() => {
          this.flash.flashSuccess(`Deleted course ${courseId}`);
        });

      }, (error: any) => this.flash.handleError(error)));
  }

  resetForm() {
    this.updateFormData.reset();
    this.initFormValues();
  }

  userCanUpdateCourse() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
