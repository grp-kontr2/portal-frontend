import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-resource',
  templateUrl: './course-resource.component.html'
})
export class CourseResourceComponent implements OnInit, OnDestroy {
  @Input() collection: Array<any>;
  @Input() path: string;

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }
}
