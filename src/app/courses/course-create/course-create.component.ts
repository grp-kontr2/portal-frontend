import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CourseService} from '../../services/course.service';
import {Subscription} from 'rxjs';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.scss']
})
export class CourseCreateComponent implements OnInit, OnDestroy {

  createData: FormGroup;
  subscriptions: Subscription[] = [];
  constructor(private courseService: CourseService,
              private router: Router,
              private fb: FormBuilder,
              private flash: FlashService) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: ['', Validators.required],
      codename: ['', Validators.required],
      description: [''],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('[CREATE] Course create form data: ', data);
    this.subscriptions.push(this.courseService.createCourse(data['name'], data['codename'], data['description']).subscribe(
      () => {
        this.router.navigateByUrl('/courses').then(() => {
          this.flash.flashSuccess(`Created course ${data['codename']}.`);
        });
      }, (error: any) => {
        this.flash.flashDanger(`Error creating course ${data['codename']}.`);
        this.flash.handleError(error);
      }));
  }
}
