import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';
import { FlashService } from '../services/flash.service';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private flash: FlashService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const loggedInUser = this.authService.getLoggedInUser();
    if (loggedInUser.is_admin) {
      return true;
    }
    this.router.navigateByUrl('/dashboard').then(() => {
      this.flash.flashDanger(`You are not authorized to access ${next.url}.`);
    });
  }
}
