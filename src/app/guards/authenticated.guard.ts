import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, CanLoad, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from '../services/auth.service';


import { User } from '../shared/models/models';
import { catchError, map, tap } from 'rxjs/operators';
import { FlashService } from '../services/flash.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private authService: AuthService, private router: Router, private flash: FlashService) {
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    const url: string = state.url;
    const loginCheck = await this.checkLogin(url);

    if (loginCheck) {
      await this.refreshUser(url);
      return true;
    }

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;
    console.log('[GUARD] Set authService redirect url in authenticated guard to ', this.authService.redirectUrl);
    this.authService.logout();
    this.redirectToLogin(url);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(childRoute, state);
  }

  async canLoad(route: Route): Promise<boolean> {
    return await this.checkLogin(route.path);
  }

  private async checkLogin(url: string): Promise<boolean> {
    console.log('[GUARD] Checking login for route ', url);
    if (this.authService.hasValidAccessToken()) {
      console.log('[GUARD] Login check successful.');
      return true;
    }

    if (this.authService.hasValidRefreshToken()) {
      return await this.authService.refreshAccessToken().pipe(
        map(() => {
          return true;
        }), catchError(error => {
          console.error('[GUARD] Error at refreshing token: ', error);
          this.flash.handleError(error);
          this.redirectToLogin(url);
          return of(false);
        })
      ).toPromise();
    } else {
      return false;
    }

  }

  private redirectToLogin(url: string) {
    this.router.navigateByUrl('/login').then(
      () => {
        console.log('[GUARD] redirected to \'/login\' from authenticated-guard');
        this.flash.flashDanger(`You need to log in to access the requested page \'${url}\'`);
      }, (error: any) => {
        console.error('[GUARD] Navigation to \'/login\' failed: ', error);
        this.flash.handleError(error);
      });
  }

  async refreshUser(url: string = '') {
    let user = this.authService.getLoggedInUser();
    console.log('[GUARD] got user ', user);
    if (user == null || user === {} as User || user.permissions == null || user.permissions === {}) {
      user = await this.authService.initUser();
      console.log('[GUARD] updated user: ', user);
      if (user == null) {
        this.authService.clearAuthData();
        this.authService.clearCookies();
        this.redirectToLogin(url);
      }
    }
  }
}
