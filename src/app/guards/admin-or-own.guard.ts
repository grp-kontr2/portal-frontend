import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';
import { FlashService } from '../services/flash.service';

@Injectable()
export class AdminOrOwnGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private flash: FlashService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const loggedInUser = this.authService.getLoggedInUser();
    if (loggedInUser.is_admin) {
      console.log('[GUARD] Logged in user is admin -> ALLOW');
      return true;
    }
    const userId = next.params['uid'];
    if (loggedInUser.id === userId || loggedInUser.username === userId) {
      console.log('[GUARD] Logged in user owns the user profile -> ALLOW');
      return true;
    }

    // for authorization for creating secrets
    const targetId = next.params['clientId'];
    if (loggedInUser.id === targetId || loggedInUser.codename === targetId) {
      console.log('[GUARD] Logged in user owns the secrets -> ALLOW');
      return true;
    }

    console.log('[GUARD] Logged in user is not admin nor the client -> REDIRECT TO Dashboard');
    this.router.navigateByUrl('/dashboard').then(() => {
      this.flash.flashDanger(`You are not authorized to access ${next.url}.`);
    });
  }
}
