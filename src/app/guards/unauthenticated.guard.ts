import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';
import { FlashService } from '../services/flash.service';

@Injectable()
export class UnauthenticatedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private flash: FlashService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin(state.url);
  }

  private checkLogin(url: string): boolean {
    console.log('Checking login for route ', url, ' - must be unauthenticated');
    if (this.authService.hasValidAccessToken()) {
      console.log('[GUARD] Login check successful, redirecting to dashboard');
      this.router.navigateByUrl('/dashboard').then(() => {
        this.flash.flashDanger('You are already logged in.');
      });
    } else {
      console.error('[GUARD] Login check unsuccessful, staying on login page');
      this.authService.clearAuthData();
      return true;
    }
  }
}
