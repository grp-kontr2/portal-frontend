import { Component, OnDestroy, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-gitlab-oauth-result',
  templateUrl: './gitlab-oauth-result.component.html',
  styleUrls: [ './gitlab-oauth-result.component.scss' ]
})
export class GitlabOauthResultComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];

  constructor(
    private cookies: CookieService,
    private auth: AuthService,
    private userService: UserService,
    private router: Router,
    private flash: FlashService
  ) {
  }

  ngOnInit() {
    console.log('processing gitlab oauth login');
    this.doLogin();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doLogin() {
    const gitlab_token = this.cookies.get('gitlab_token');
    const username = this.cookies.get('username');
    console.log('Gitlab username: ', username);
    console.log('Gitlab token: ', gitlab_token);
    this.subscriptions.push(this.auth.oauthLogin(username, gitlab_token).pipe(
      map(() => {
        this.router.navigateByUrl('/dashboard').then(
          () => {
            this.flash.flashSuccess('Login successful.');
          });
      }),
      catchError(error => {
        console.log(`[LOGIN] GitLab, username ${username}, token ${gitlab_token} error: `, error);
        this.flash.flashDanger('OAuth login using GitLab failed.');
        this.flash.handleError(error);
        return of(null);
      })).subscribe());
  }

}
