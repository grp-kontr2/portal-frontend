import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GitlabOauthResultComponent } from './gitlab-oauth-result.component';

describe('GitlabOauthResultComponent', () => {
  let component: GitlabOauthResultComponent;
  let fixture: ComponentFixture<GitlabOauthResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitlabOauthResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitlabOauthResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
