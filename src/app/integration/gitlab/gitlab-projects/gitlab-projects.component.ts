import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GitlabProject, User } from '../../../shared/models/models';
import { GitlabService } from '../../../services/gitlab.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../../services/flash.service';


@Component({
  selector: 'app-gitlab-projects',
  templateUrl: './gitlab-projects.component.html',
  styleUrls: [ './gitlab-projects.component.scss' ]
})
export class GitlabProjectsComponent implements OnInit, OnDestroy {

  projects: GitlabProject[] = [];
  temp: GitlabProject[] = [];
  subscriptions: Subscription[] = [];
  loggedInUser: User;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator = false;


  constructor(private glService: GitlabService,
              private route: ActivatedRoute,
              private cookies: CookieService,
              public flash: FlashService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      this.loadProjects();
    }));
  }

  public enableGLProject(project: GitlabProject) {
    const p_name = project.path_with_namespace;
    this.subscriptions.push(this.glService.enableGitlabProject(p_name).subscribe((p) => {
        console.log('Enable kontr for project: ', p);
        const text = `Project ${p_name} enabled to use the Kontr.`;
        this.flash.flashSuccess(text);
        this.loadProjects();
      },
      (err) => {
        this.universalErrorHandler(err);
      }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private loadProjects() {
    this.loadingIndicator = true;
    this.subscriptions.push(this.glService.listGitlabProjects().subscribe((projects) => {
      console.log('[PROJECTS] Load projects: ', projects);
      this.projects = projects;
      this.temp = [ ...projects ];
      const text = `Project list has been reloaded: ${projects.length} projects found`;
      this.loadingIndicator = false;
      this.flash.flashSuccess(text);
    }, (err) => {
      this.universalErrorHandler(err);
    }));
  }

  private glEnableServiceToken() {
    const subscription = this.glService.getServiceToken().subscribe((response: Response) => {
      console.log('[RES] Response: ', response);
      Object.keys(response).forEach((name) => {
        const value = response[ name ];
        this.cookies.set(name, value, undefined, '/');
        console.log(`[COOKIE] Setting cookie: ${name} -> ${value}`);
      });
      const text = `Service token received - ${response[ 'gitlab_username' ]}`;
      this.flash.flashSuccess(text);
      this.loadProjects();
    }, (err) => {
      this.universalErrorHandler(err);
    });
    this.subscriptions.push(subscription);
  }

  private universalErrorHandler(err: any) {
    console.error('[ERR] Processing error: ', err);
    this.flash.handleError(err);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(d => d.path_with_namespace.toLowerCase().indexOf(val) !== -1 || !val);

    // update the rows
    this.projects = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
