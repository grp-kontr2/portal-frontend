import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntegrationRoutingModule } from './integration-routing.module';
import { GitlabProjectsComponent } from './gitlab/gitlab-projects/gitlab-projects.component';
import { GitlabComponent } from './gitlab/gitlab.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    GitlabProjectsComponent,
    GitlabComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    IntegrationRoutingModule
  ]
})
export class IntegrationModule { }
