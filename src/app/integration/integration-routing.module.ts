import { RouterModule, Routes } from '@angular/router';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';
import { NgModule } from '@angular/core';
import { GitlabComponent } from './gitlab/gitlab.component';
import { AuthenticatedGuard } from '../guards/authenticated.guard';
import { GitlabProjectsComponent } from './gitlab/gitlab-projects/gitlab-projects.component';

const routes: Routes = [
  {
    path: '',
    component: GitlabComponent,
    pathMatch: 'full',
    canActivate: [ AuthenticatedGuard ],
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: 'gitlab',
    pathMatch: 'full',
    component: GitlabComponent,
    canActivate: [ AuthenticatedGuard ],
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: 'gitlab/projects',
    pathMatch: 'full',
    component: GitlabProjectsComponent,
    canActivate: [ AuthenticatedGuard ],
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class IntegrationRoutingModule {
}
