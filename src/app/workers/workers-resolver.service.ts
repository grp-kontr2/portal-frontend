import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Worker } from '../shared/models/models';
import { WorkerService } from '../services/worker.service';
import { map } from 'rxjs/operators';
import { FlashService } from '../services/flash.service';

@Injectable()
export class WorkersResolver implements Resolve<Worker[]> {
  constructor(public service: WorkerService,
              private router: Router,
              private flash: FlashService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Worker[]> | Promise<Worker[]> | Worker[] {
    return this.service.listWorkers().pipe(map(workers => {
        if (workers) {
          console.log('Workers: ', workers);
          return workers;
        } else {
          this.router.navigateByUrl('/').then(
            () => {
              this.flash.flashDanger(`Workers not found.`);
            }, (error: any) => {
              console.error('[RESOLVE] Could not navigate to workers list: ', error.error);
              this.flash.handleError(error);
            });
          return null;
        }
      })
    );
  }
}
