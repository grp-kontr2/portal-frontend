import { RouterModule, Routes } from '@angular/router';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';
import { WorkerListComponent } from './worker-list/worker-list.component';
import { AdminGuard } from '../guards/admin.guard';
import { WorkerCreateComponent } from './worker-create/worker-create.component';
import { WorkerDetailComponent } from './worker-detail/worker-detail.component';
import { NgModule } from '@angular/core';
import { WorkerResolver } from './worker-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: WorkerListComponent,
    canActivate: [ AdminGuard ],
    runGuardsAndResolvers: 'always',
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: 'create',
    component: WorkerCreateComponent,
    canActivate: [ AdminGuard ]
  },
  {
    path: ':wid',
    pathMatch: 'full',
    runGuardsAndResolvers: 'always',
    canActivate: [ AdminGuard ],
    component: WorkerDetailComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      worker: WorkerResolver
    }
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class WorkerRoutingModule {
}
