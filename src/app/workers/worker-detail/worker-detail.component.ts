import { Component, OnDestroy, OnInit } from '@angular/core';
import { User, Worker } from '../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkerService } from '../../services/worker.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-worker-detail',
  templateUrl: './worker-detail.component.html',
  styleUrls: [ './worker-detail.component.scss' ]
})
export class WorkerDetailComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  worker: Worker;
  loggedInUser: User;
  updateFormData: FormGroup;
  workerStatus: any;

  constructor(private route: ActivatedRoute,
              private service: WorkerService,
              private fb: FormBuilder,
              private flash: FlashService,
              private router: Router,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, worker: Worker }) => {
      this.loggedInUser = data.loggedInUser;
      this.worker = data.worker;
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    this.updateFormData.setValue({
      name: this.worker.name,
      tags: this.worker.tags,
      url: this.worker.url,
      portalSecret: this.worker.portal_secret,
    });
  }

  createForm() {
    this.updateFormData = this.fb.group({
      name: [ '', Validators.required ],
      tags: [ '' ],
      url: [ '' ],
      portalSecret: [ '' ],
    });
  }

  resetForm() {
    this.updateFormData.reset();
    this.initFormValues();
  }

  updateWorker() {
    if (this.updateFormData.status !== 'VALID') {
      console.warn('[WORKER] Invalid form submitted', this.updateFormData);
      this.flash.flashDanger('Invalid form submitted');
      return;
    }

    const data = this.updateFormData.value;
    const newValues = {
      id: this.worker.id,
      name: data[ 'name' ],
      url: data[ 'url' ],
      tags: data[ 'tags' ],
      portal_secret: data[ 'portalSecret' ],
    } as Worker;

    const updateWorkerSubscription = this.service.updateWorker(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flash.flashSuccess('Update successful.');
        });
      }, (error: any) => {
        this.flash.handleError(error);
      }
    );
    this.subscriptions.push(updateWorkerSubscription);
  }

  deleteComponent() {
    const componentId = this.worker.id;
    this.subscriptions.push(this.service.deleteWorkers(componentId).subscribe(
      () => {
        console.log('Deleted worker ', componentId);
        this.router.navigateByUrl('/workers').then(() => {
          this.flash.flashSuccess(`Deleted worker ${componentId}`);
        });

      }, (error: any) => {
        this.flash.handleError(error);
      }));
  }

}
