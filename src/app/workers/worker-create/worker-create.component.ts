import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WorkerService } from '../../services/worker.service';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-component-create',
  templateUrl: './worker-create.component.html',
  styleUrls: [ './worker-create.component.scss' ]
})
export class WorkerCreateComponent implements OnInit, OnDestroy {

  createData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private service: WorkerService,
              private router: Router,
              private fb: FormBuilder,
              private flash: FlashService) {
    this.createForm();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      url: [ '' ],
      portalSecret: [ '' ],
      tags: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted', this.createData);
      this.flash.flashDanger(`Invalid form submitted`);
      return;
    }
    console.log('[CREATE] Worker create form data: ', data);

    this.subscriptions.push(this.service.createWorker(data).subscribe(
      () => {
        this.router.navigateByUrl('/workers').then(() => {
          this.flash.flashSuccess(`Created component ${data[ 'name' ]}.`);
        });
      }, (error: any) => {
        this.flash.flashDanger(`Error creating worker ${data[ 'name' ]}.`);
        this.flash.handleError(error);
      }));
  }


}
