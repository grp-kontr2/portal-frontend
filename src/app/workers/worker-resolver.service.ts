import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Worker } from '../shared/models/models';
import { Observable } from 'rxjs';
import { WorkerService } from '../services/worker.service';
import { map, take } from 'rxjs/operators';
import { FlashService } from '../services/flash.service';


@Injectable()
export class WorkerResolver implements Resolve<Worker> {
  constructor(public service: WorkerService,
              private router: Router,
              private flash: FlashService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Worker> | Promise<Worker> | Worker {
    const wid = route.paramMap.get('wid');
    return this.service.getWorker(wid).pipe(take(1), map(
      worker => {
        if (worker) {
          return worker;
        } else {
          this.router.navigateByUrl('/workers').then(
            () => {
              this.flash.flashDanger(`Worker ${wid} not found.`);
            }, (error: any) => {
              console.error('[RESOLVE] Could not navigate to workers list: ', error.error);
              this.flash.handleError(error);
            });
          return null;
        }
      }
    ));
  }
}

