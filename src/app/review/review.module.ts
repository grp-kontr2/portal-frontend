import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { TestDetailsComponent } from './submission-tests/test-results/test-details/test-details.component';
import { TestsTableComponent } from './submission-tests/test-results/tests-table/tests-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DiffComponent } from './diff/diff.component';
import {ReviewRoutingModule} from './review-routing.module';
import {ReviewComponent} from './review.component';
import {TreeModule} from 'angular-tree-component';
import {CommonModule} from '@angular/common';
import {SubmissionResolver} from './resolvers/submission-resolver.service';
import {ReviewLayoutComponent} from './layout/layout.component';
import { SubmissionSourcesComponent } from './submission-sources/submission-sources.component';
import { SubmissionTestsComponent } from './submission-tests/submission-tests.component';
import {SharedModule} from '../shared/shared.module';
import { ReviewListComponent } from './submission-sources/review-list/review-list.component';
import { DiffEditorComponent } from './diff/diff-editor/diff-editor.component';
import { NotebooksComponent } from './notebooks/notebooks.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TestResultsComponent} from './submission-tests/test-results/test-results.component';
import { TestSourcesComponent } from './submission-tests/test-sources/test-sources.component';
import { TestOutputsComponent } from './submission-tests/test-outputs/test-outputs.component';
import { GenericTreeViewComponent } from './shared/generic-tree-view/generic-tree-view.component';
import { GenericEditorComponent } from './shared/generic-editor/generic-editor.component';

@NgModule({
  declarations: [
    ReviewComponent,
    NavbarComponent,
    TestDetailsComponent,
    TestsTableComponent,
    DiffComponent,
    ReviewLayoutComponent,
    SubmissionSourcesComponent,
    SubmissionTestsComponent,
    ReviewListComponent,
    DiffEditorComponent,
    NotebooksComponent,
    TestResultsComponent,
    TestResultsComponent,
    TestSourcesComponent,
    TestOutputsComponent,
    GenericTreeViewComponent,
    GenericEditorComponent
  ],
  imports: [
    CommonModule,
    ReviewRoutingModule,
    HttpClientModule,
    NgxDatatableModule,
    TreeModule.forRoot(),
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    SubmissionResolver
  ],
  bootstrap: [ReviewComponent]
})
export class ReviewModule { }
