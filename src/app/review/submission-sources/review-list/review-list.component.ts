import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TreeViewService} from '../../services/tree-view.service';
import {ReviewItem, User} from '../../../shared/models/models';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ReviewService} from '../../services/review.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from '../../../services/auth.service';
import {PermissionsService} from '../../../services/permissions.service';
import {EditorService} from '../../services/editor.service';
import {NotebookService} from '../../services/notebook.service';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})

export class ReviewListComponent implements OnInit, OnDestroy {

  @Input() review: any;
  @Input() submission: any;
  @Input() tree: any;
  loggedInUser: User;
  subscriptions: Subscription[] = [];
  createReviewItemForm: FormGroup;
  editReviewItemForm: FormGroup;
  reviewListForm: FormGroup;
  exportISNotebooksForm: FormGroup;
  reviewItems = [];
  filesForReview = [];
  selectedFile: string;
  order = 0;
  selectedReviews: ReviewItem[] = [];
  selected = null;


  constructor(private treeViewService: TreeViewService,
              private reviewService: ReviewService,
              private editorService: EditorService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              public auth: AuthService,
              private router: Router,
              private permissions: PermissionsService,
              private notebookService: NotebookService) {

    this.createReviewItemForm = formBuilder.group({
      content: [''],
      file: '',
      firstLine: null,
      lastLine: null
    });
    this.editReviewItemForm = formBuilder.group({
      content: [''],
      file: '',
      firstLine: null,
      lastLine: null
    });
    this.reviewListForm = formBuilder.group({
      order: ['']
    });
    this.exportISNotebooksForm = formBuilder.group( {
      content: ['']
    });
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
    }));
    this.parseReviewItems();
    this.getFileForDropdown();
    this.subscriptions.push(this.editorService.submissionFilePathSent$.subscribe(data => {
      this.selectedFile = data['path'];
      console.log(this.selectedFile);
    }));
    this.subscriptions.push(this.editorService.selectionSent$.subscribe( data => {
      console.log('SELECTION', data);
      this.fillTheForm(data);
    }));
  }

  private fillTheForm(data) {
    const startLine = data[0];
    const endLine = data[1];
    if (startLine !== endLine) {
      this.createReviewItemForm.controls['firstLine'].setValue(startLine);
      this.createReviewItemForm.controls['lastLine'].setValue(endLine);
    } else {
      this.createReviewItemForm.controls['firstLine'].setValue(null);
      this.createReviewItemForm.controls['lastLine'].setValue(null);
    }
    if (this.selectedFile) {
      const path = this.selectedFile[0] === '.' ? this.selectedFile.slice(2, this.selectedFile.length) : this.selectedFile;
      this.createReviewItemForm.controls['file'].setValue(path);
    }
  }

  parseReviewItems() {
    this.subscriptions.push(this.reviewService.getReview(this.submission.id).subscribe( review => {
      this.review = review;
      this.reviewItems = [];
      if (this.review && this.review.review_items) {
        for (const item of this.review.review_items) {
          this.subscriptions.push(this.reviewService.getReviewItemVersions(this.submission, item).subscribe(versions => {
            this.reviewItems.push({review: item, versions: versions});
          }));
        }
      }
    }));
    console.log(this.reviewItems);
  }

  focusOnReviewedFile(url: string, firstLine: number, lastLine: number) {
    console.log(url);
    if (url !== '') {
      const path = url.includes('/') ? url : './' + url;
      console.log(path);
      this.treeViewService.activateNode(path);
    }
    this.editorService.highlightBlock(firstLine, lastLine);
  }

  canWriteReview(): boolean {
    return this.permissions.checkReviewSubmission(this.loggedInUser, this.submission.course);
  }

  canEditReview(item): boolean {
    return this.loggedInUser.is_admin || this.loggedInUser.id === item.user.id;
  }

  canDeleteReview(item): boolean {
    return this.loggedInUser.is_admin || this.loggedInUser.id === item.user.id;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createReviewItem(): void {
    const content = this.createReviewItemForm.value['content'];
    const firstLine = this.createReviewItemForm.value['firstLine'];
    const lastLine = this.createReviewItemForm.value['lastLine'];
    const file = this.createReviewItemForm.value['file'] === '' ? null : this.createReviewItemForm.value['file'];
    console.log('Created new review item.', content);
    this.subscriptions.push(this.reviewService.createReviewItem(this.submission, content, file, firstLine, lastLine).subscribe(
      () => {
        console.log('Created new review item.', content);
        this.createReviewItemForm.reset();
        this.parseReviewItems();
      }));
  }

  deleteReviewItem(item: ReviewItem): void {
    this.subscriptions.push( this.reviewService.deleteReviewItem(this.submission, item).subscribe(
      () => {
        console.log('Deleted item: ', item);
        this.parseReviewItems();
      }));
  }

  updateReviewItem(item: ReviewItem): void {
    console.log('Updated item ', item);
    const newContent = this.editReviewItemForm.value['content'];
    const newFile = this.editReviewItemForm.value['file'];
    const newFirstLine = this.editReviewItemForm.value['firstLine'];
    const newLastLine = this.editReviewItemForm.value['lastLine'];
    const newData = {
      id: item.id,
      user: item.user,
      content: newContent,
      line: item.line,
      line_start: newFirstLine,
      line_end: newLastLine,
      file: newFile,
      review: item.review
    } as ReviewItem;
    this.subscriptions.push(this.reviewService.updateReviewItem(this.submission, newData).subscribe( () => {
      console.log('Edited item: ', item);
      this.parseReviewItems();
    }));
  }

  initEditReviewFormValue(item: ReviewItem) {
    console.log(item.content);
    this.editReviewItemForm.setValue({
      content: item.content,
      file: item.file,
      firstLine: item.line_start,
      lastLine: item.line_end
    });
  }

  getFileForDropdown() {
    this.filesForReview = this.parseTree(this.tree);
  }

  private parseTree(tree) {
    let result = [];
    for (const item of tree) {
      if (item['url'] !== '') {
        console.log(item);
        result.push(item);
      } else {
        const treeResult = this.parseTree(item['children']);
        result = result.concat(treeResult);
      }
    }
    return result;
  }

  orderItems() {
    this.order = 1 - this.order;
    this.reviewItems.sort((a, b) => {
      if (a.updated_at > b.updated_at) {
        return -1;
      }
      if (a.updated_at < b.updated_at) {
        return 1;
      }
      return 0;
    });

    if (this.order === 1) {
      this.reviewItems = this.reviewItems.reverse();
    }
    console.log(this.reviewItems);
  }

  addToSelectedReviews(review: ReviewItem) {
    if (!this.selectedReviews.includes(review)) {
      this.selectedReviews.push(review);
    } else {
      this.selectedReviews = this.selectedReviews.filter(item => item !== review);
    }
    console.log(this.selectedReviews);
  }

  exportToISNotebooks() {
    if (this.selectedReviews.length === 0) {
      return;
    }
    let exp = '=== Kontr 2 Review Tool\n\n';
    for (const item of this.selectedReviews) {
      let str = '';
      const content = item['content'];
      const firstLine = item['line_start'];
      const lastLine = item['line_end'];
      const file = item['file'];
      const author = item.user.name;
      if (author) {
        str = str + 'Author: ' + author + ' | ';
      }
      if (file) {
        str = str + 'File: ' + file;
        if (firstLine && lastLine) {
          str = str + ' | First line: ' + firstLine + ' | Last line: ' + lastLine;
        }
      }
      str = str + '\n';
      str = str + content + '\n';
      exp = exp + str + '\n';
    }
    exp = exp + '=== Kontr 2 Review Tool\n';
    console.log('[Review Tool IS Notebooks]', exp);
    this.exportISNotebooksForm.controls['content'].setValue(exp);
  }

  writeToNotebook() {
      const content = this.exportISNotebooksForm.value['content'];
      let actual;
      this.subscriptions.push(this.notebookService.getNotebook(this.submission.id).subscribe( data => {
        actual = data[this.submission.user.uco];
        let newContent = '';
        if (actual) {
          console.log(actual);
          const parsed = actual.split('=== Kontr 2 Review Tool');
          if (parsed.length !== 3) {
            newContent = actual + content;
          } else {
            console.log(parsed);
            newContent = parsed[0] + content + parsed[2];
          }
          console.log(newContent);
          this.subscriptions.push(this.notebookService.postNotebook(this.submission.id, newContent).subscribe( () => {
            console.log('Updated the IS notebook content', newContent);
          }));
        }
      }));
  }

}
