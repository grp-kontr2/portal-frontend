import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmissionSourcesComponent } from './submission-sources.component';

describe('SubmissionSourcesComponent', () => {
  let component: SubmissionSourcesComponent;
  let fixture: ComponentFixture<SubmissionSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmissionSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmissionSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
