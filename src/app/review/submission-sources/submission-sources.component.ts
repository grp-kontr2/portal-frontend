import { Component, OnInit } from '@angular/core';
import {Submission, User} from '../../shared/models/models';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {SubmissionService} from '../../services/submission.service';
import {PermissionsService} from '../../services/permissions.service';
import {ReviewService} from '../services/review.service';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-submission-sources',
  templateUrl: './submission-sources.component.html',
  styleUrls: ['./submission-sources.component.scss']
})
export class SubmissionSourcesComponent implements OnInit {
  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];
  sourcesTree = null;
  review = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService,
              private reviewService: ReviewService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      if (data.submission === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
      console.log('[SUB] Submission: ', this.submission);
      this.loadTree();
      this.loadReview();
    }));
  }

  loadTree() {
    this.subscriptions.push(this.service.getTree(this.submission.id, 'sources').subscribe(tree => {
      this.sourcesTree = this.parseTree(tree);
      console.log(this.sourcesTree);
    }));
  }

  private parseTree(tree) {
    const result = [];
    for (const [key, value] of Object.entries(tree)) {
      const node = {
        name: key,
        url: (typeof(value) === 'string') ? value : '',
        content: ''
      };

      if (typeof value === 'string') {
        node['url'] = value;
      } else {
        node['children'] = this.parseTree(value);
      }
      result.push(node);
    }
    return result;
  }

  loadReview() {
    this.subscriptions.push(this.reviewService.getReview(this.submission.id).subscribe(review => {
      this.review = review;
    }));
  }

}
