import {Component, OnDestroy, OnInit} from '@angular/core';
import {Submission, User} from '../../shared/models/models';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {SubmissionService} from '../../services/submission.service';
import {PermissionsService} from '../../services/permissions.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-diff',
  templateUrl: './diff.component.html',
  styleUrls: ['./diff.component.scss']
})
export class DiffComponent implements OnInit, OnDestroy {
  firstSubmission: Submission;
  secondSubmission: Submission;
  subscriptions: Subscription[] = [];
  loggedInUser: User;
  sources = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: {
      loggedInUser: User, firstSubmission: Submission, secondSubmission: Submission }) => {
      if (data.firstSubmission === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.firstSubmission = data.firstSubmission;
      this.secondSubmission = data.secondSubmission;
      console.log('[DIFF] First (left) submission: ', this.firstSubmission);
      console.log('[DIFF] Second (right) submission: ', this.secondSubmission);
      this.loadTree();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  loadTree() {
    this.subscriptions.push(this.service.getTree(this.firstSubmission.id, 'sources').subscribe(tree => {
      if (tree === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Cannot load diff tree..`, { cssClass: 'alert-danger' });
        });
      }
      this.sources = this.parseTree(tree);
      console.log(tree);
    }));
  }

  private parseTree(tree) {
    const result = [];
    for (const [key, value] of Object.entries(tree)) {
      const node = {
        name: key,
        url: (typeof(value) === 'string') ? value : ''
      };

      if (typeof value === 'string') {
        node['url'] = value;
      } else {
        node['children'] = this.parseTree(value);
      }
      result.push(node);
    }
    return result;
  }

}
