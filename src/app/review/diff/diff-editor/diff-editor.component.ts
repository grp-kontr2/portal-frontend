import {Component, OnDestroy, OnInit} from '@angular/core';
import {Submission, User} from '../../../shared/models/models';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {SubmissionService} from '../../../services/submission.service';
import {PermissionsService} from '../../../services/permissions.service';
import {DiffService} from '../../services/diff.service';
import * as monaco from 'monaco-editor';

@Component({
  selector: 'app-diff-editor',
  templateUrl: './diff-editor.component.html',
  styleUrls: ['./diff-editor.component.scss']
})
export class DiffEditorComponent implements OnInit, OnDestroy{
  firstSubmission: Submission;
  secondSubmission: Submission;
  subscriptions: Subscription[] = [];
  loggedInUser: User;
  diffEditor;
  path;
  inline = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService,
              private diffService: DiffService
  ) {
  }


  ngOnInit() {
    this.initializeMonacoDiff();
    this.subscriptions.push(this.route.data.subscribe((data: {
      loggedInUser: User, firstSubmission: Submission, secondSubmission: Submission }) => {
      if (data.firstSubmission === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.firstSubmission = data.firstSubmission;
      this.secondSubmission = data.secondSubmission;
    }));
    this.subscribeToDiffService();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initializeMonacoDiff() {
    const originalModel = monaco.editor.createModel('', 'text');
    const modifiedModel = monaco.editor.createModel('', 'text');

    this.diffEditor = monaco.editor.createDiffEditor(document.getElementById('container-diff'), {
      automaticLayout: true,
    });
    this.diffEditor.setModel({
      original: originalModel,
      modified: modifiedModel
    });

    this.diffEditor.getOriginalEditor().updateOptions({readOnly: false});
    this.diffEditor.getModifiedEditor().updateOptions({readOnly: false});
  }


  private subscribeToDiffService() {
    this.subscriptions.push(this.diffService.selectedFileChanged$.subscribe(path => {
      if (path === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`File path not available`, { cssClass: 'alert-danger' });
        });
      }
      console.log('[DIFF] File path: ', path);
      this.path = path;
      this.loadFiles(path);
      this.setLanguage();
    }));
  }

  reloadFiles() {
    if (this.path) {
      this.loadFiles(this.path);
    }
  }

  private loadFiles(path: string) {
    this.subscriptions.push(this.service.downloadSingleFile(this.firstSubmission.id, 'sources', path).subscribe(content => {
      if (content === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`File content not available`, { cssClass: 'alert-danger' });
        });
      }
      console.log(`[DIFF] Left editor content: `, content);
      this.diffEditor.getOriginalEditor().getModel().setValue(content);
    }));
    this.subscriptions.push(this.service.downloadSingleFile(this.secondSubmission.id, 'sources', path).subscribe(content => {
      if (content === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`File content not available`, { cssClass: 'alert-danger' });
        });
      }
      console.log(`[DIFF] Right editor content: `, content);
      this.diffEditor.getModifiedEditor().getModel().setValue(content);
    }));
  }

  setInline() {
    this.diffEditor.updateOptions({'renderSideBySide': this.inline});
    this.inline = !this.inline;
  }

  private setLanguage() {
    const languages = monaco.languages.getLanguages();
    const extension = '.' + this.path.split('.').pop();
    for (const language of languages) {
      if (language.extensions.includes(extension)) {
        monaco.editor.setModelLanguage(this.diffEditor.getOriginalEditor().getModel(), language.id);
        monaco.editor.setModelLanguage(this.diffEditor.getModifiedEditor().getModel(), language.id);
        console.log(`[DIFF]: File extension: ${extension}. Language of both models set to ${language.id}`);
      }
    }
  }

}
