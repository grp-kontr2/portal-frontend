import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../shared/models/models';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { ManagementService } from '../../services/management.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-review-layout',
  templateUrl: './layout.component.html',
  styleUrls: [ './layout.component.scss' ]
})
export class ReviewLayoutComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  subscriptions: Subscription[] = [];
  status: boolean;

  constructor(private auth: AuthService,
              private route: ActivatedRoute,
              private managementService: ManagementService,
              private flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
    }));
    this.subscriptions.push(this.getStatusPeriodically());
  }

  private getStatus() {
    this.subscriptions.push(this.managementService.getPortalStatus().subscribe((status) => {
      console.log(`[STATUS] Status:`, status);
      this.status = status.ok;
      if (!this.status) {
        this.flashMessagesService.show('Backend is not running - please contact the administrator!',
          { timeout: 60000, cssClass: 'alert-danger' });
      }
    }, (err) => { this.status = false; }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private getStatusPeriodically() {
    return timer(0, 60000).subscribe(() => this.getStatus());
  }
}
