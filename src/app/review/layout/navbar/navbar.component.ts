import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public sid = null;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    const url = this.router.url;
    const parts = url.split('/');
    this.sid = parts[2];
  }

}
