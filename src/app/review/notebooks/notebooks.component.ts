import { Component, OnInit } from '@angular/core';
import {Submission, User} from '../../shared/models/models';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {NotebookService} from '../services/notebook.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PermissionsService} from '../../services/permissions.service';

@Component({
  selector: 'app-notebooks',
  templateUrl: './notebooks.component.html',
  styleUrls: ['./notebooks.component.scss']
})
export class NotebooksComponent implements OnInit {
  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];
  notebook = null;
  editNotebookForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private notebookService: NotebookService,
              private formBuilder: FormBuilder,
              private permissions: PermissionsService
  ) {
    this.editNotebookForm = formBuilder.group({
      content: ['']
    });
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      if (data.submission === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
    }));

    this.subscriptions.push(this.notebookService.getNotebook(this.submission.id).subscribe( notebook => {
      console.log('[Review Tool - Notebooks] Loaded notebook:', notebook);
      this.notebook = notebook;
    }));
  }

  initEditNotebookForm() {
    if (this.notebook && this.submission) {
      this.editNotebookForm.setValue({
        content: this.notebook[this.submission.user.uco]
      });
    }
  }

  updateNotebook() {
    const content = this.editNotebookForm.value['content'];
    this.subscriptions.push(this.notebookService.postNotebook(this.submission.id, content).subscribe( () => {
      console.log('Updated the IS notebook content', content);
      this.subscriptions.push(this.notebookService.getNotebook(this.submission.id).subscribe( notebook => {
        console.log('[Review Tool - Notebooks] Loaded notebook:', notebook);
        this.notebook = null;
        this.notebook = notebook;
      }));
    }));
  }

  canEditNotebooks() {
    return this.loggedInUser.is_admin || this.permissions.checkReadProjects(this.loggedInUser, this.submission.course);
  }

}
