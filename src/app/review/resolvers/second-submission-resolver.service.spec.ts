import { TestBed } from '@angular/core/testing';

import { SecondSubmissionResolver } from './second-submission-resolver.service';

describe('SecondSubmissionResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecondSubmissionResolver = TestBed.get(SecondSubmissionResolver);
    expect(service).toBeTruthy();
  });
});
