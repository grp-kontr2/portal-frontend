import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Submission} from '../../shared/models/models';
import {SubmissionService} from '../../services/submission.service';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirstSubmissionResolver implements Resolve<Submission> {

  constructor(private submissionService: SubmissionService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Submission> | Promise<Submission> | Submission {
    const sid = route.paramMap.get('first');
    return this.submissionService.getSubmission(sid).pipe(take(1), map(submission => {
        if (submission) {
          console.log(`[DIFF] Resolving first submission: ${sid}: `, submission);
          return submission;
        } else {
          console.log(`[DIFF] Cannot resolve first submission: ${sid}: `, submission);
          return null;
        }
      })
    );
  }
}
