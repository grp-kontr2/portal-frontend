import { TestBed } from '@angular/core/testing';

import { FirstSubmissionResolver } from './first-submission-resolver.service';

describe('FirstSubmissionResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FirstSubmissionResolver = TestBed.get(FirstSubmissionResolver);
    expect(service).toBeTruthy();
  });
});
