import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {IActionMapping, ITreeOptions, TREE_ACTIONS} from 'angular-tree-component';
import {Submission, User} from '../../../shared/models/models';
import {Subscription} from 'rxjs';
import {SubmissionService} from '../../../services/submission.service';
import {ActivatedRoute} from '@angular/router';
import {EditorService} from '../../services/editor.service';
import {DiffService} from '../../services/diff.service';
import {TreeViewService} from '../../services/tree-view.service';

const actionMapping: IActionMapping = {
  mouse: {
    click: (tree, node, $event) => {
      if (node.hasChildren) {
        TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
      } else {
        if (!node.isActive) {
          TREE_ACTIONS.ACTIVATE(tree, node, $event);
        } else {
          TREE_ACTIONS.DEACTIVATE(tree, node, $event);
        }
      }
    }
  }
};

@Component({
  selector: 'app-generic-tree-view',
  templateUrl: './generic-tree-view.component.html',
  styleUrls: ['./generic-tree-view.component.scss']
})

export class GenericTreeViewComponent implements OnInit {
  @ViewChild('tree') tree;
  @Input() component: string;
  @Input() treeView: any;
  nodes = [];


  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];

  options: ITreeOptions = {
    actionMapping: actionMapping,
    allowDrag: false, // important
    allowDrop: false // important
  };

  constructor(private service: EditorService, private submissionService: SubmissionService, private route: ActivatedRoute, private diffService: DiffService, private treeViewService: TreeViewService) { }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
    }));

    if (this.treeView) {
      this.nodes = this.treeView;
    }

    this.treeViewService.nodePathSent$.subscribe(data => {
      console.log('DATA', data);
      const node = this.tree.treeModel.getNodeBy(item => item.data.url === data);
      if (node) {
        console.log('ACTIVATE', node);
        node.setIsActive(true, false);
        this.sendPathToEditor();
      }
    });
  }

  sendPathToEditor() {
    const activeNode = this.tree.treeModel.getActiveNode();

    if (!activeNode) {
      console.error('Cannot send file path from an undefined node.');
      return;
    }

    if (!activeNode.data || !activeNode.data.url) {
      const undefinedField = !activeNode.data ? 'data' : 'url';
      console.error(`The active node has an undefined ${undefinedField} field. Cannot send the file path.`);
      return;
    }

    const path = activeNode.data.url;

    if (this.component.toLowerCase() === 'diff') {
      this.diffService.changeSelectedFile(activeNode.data.url);
      console.log(`[DIFF TREE] Path ${activeNode.data.url} sent to data service from node #${activeNode.id} '${activeNode.data.name}'.`);
      return;
    }

    if (activeNode.data.content === '') {
      this.subscriptions.push(this.submissionService.downloadSingleFile(this.submission.id, this.component, path).subscribe(content => {
        activeNode.data.content = content;
        const result = {
          content: activeNode.data.content,
          path: activeNode.data.url
        };
        this.sendDataToEditor(result);
      }));
    } else {
      const result = {
        content: activeNode.data.content,
        path: activeNode.data.url
      };
      this.sendDataToEditor(result);
    }
  }

  searchFile(fileName: string) {
    this.tree.treeModel.filterNodes(fileName);
  }

  private sendDataToEditor(result) {
    if (this.component.toLowerCase() === 'results') {
      this.service.sendOutputPathToEditor(result);
    }
    if (this.component.toLowerCase() === 'test_files') {
      this.service.sendTestPathToEditor(result);
    }
    if (this.component.toLowerCase() === 'sources') {
      this.service.sendSubmissionFilePathToEditor(result);
    }
  }
}
