import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericTreeViewComponent } from './generic-tree-view.component';

describe('GenericTreeViewComponent', () => {
  let component: GenericTreeViewComponent;
  let fixture: ComponentFixture<GenericTreeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericTreeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericTreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
