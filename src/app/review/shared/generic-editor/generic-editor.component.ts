import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Submission, User} from '../../../shared/models/models';
import {Subscription} from 'rxjs';
import {TestService} from '../../services/test.service';
import {SubmissionService} from '../../../services/submission.service';
import {ActivatedRoute} from '@angular/router';
import * as monaco from 'monaco-editor';
import {EditorService} from '../../services/editor.service';

@Component({
  selector: 'app-generic-editor',
  templateUrl: './generic-editor.component.html',
  styleUrls: ['./generic-editor.component.scss']
})
export class GenericEditorComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() component: string = null;
  editor;
  path: string = null;
  decorations;

  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];

  constructor(private service: EditorService, private submissionService: SubmissionService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
    }));
    this.subscriptions.push(this.service.highlightBlockSent$.subscribe(data => {
      if (data) {
        const first = data[0];
        const last = data[1];
        this.highlightLines(first, last);
      }
    }));
  }

  ngAfterViewInit() {
    if (this.component) {
      this.editor = monaco.editor.create(document.getElementById(this.component), {
        value: '',
        language: 'text',
        readOnly: true,
        glyphMargin: true,
        automaticLayout: true,
        scrollbar: {
          horizontal: 'visible',
          horizontalScrollbarSize: 15,
          verticalScrollbarSize: 10
        }
      });
      if (this.component === 'results') {
        this.service.outputFilePathSent$.subscribe(data => {
          this.path = data['path'];
          const content = data['content'];
          this.editor.getModel().setValue(content);
          this.setLanguage(this.path);
        });
      }
      if (this.component === 'test_files') {
        this.service.testFilePathSent$.subscribe(data => {
          this.path = data['path'];
          const content = data['content'];
          this.editor.getModel().setValue(content);
          this.setLanguage(this.path);
        });
      }
      if (this.component === 'sources') {
        this.service.submissionFilePathSent$.subscribe(data => {
          this.path = data['path'];
          const content = data['content'];
          this.editor.getModel().setValue(content);
          this.setLanguage(this.path);
        });
        this.editor.addAction(this.customAction());
      }
    }
  }

   private customAction() {
    return {
      id: 'OPEN_MODAL_WINDOW',
      label: 'Add Review to Selected Block',
      contextMenuGroupId: 'navigation',
      run: (ed) => {
        const position = this.editor.getSelection();
        const startLine = position.startLineNumber;
        const endLine = position.endLineNumber;
        this.reviewBlockOfCode(startLine, endLine);
      },
    };
  }

  private reviewBlockOfCode(startLine: number, endLine: number) {
    this.service.sendSelectionFromEditor(startLine, endLine);
  }

  private setLanguage(path: string) {
    const languages = monaco.languages.getLanguages();
    const extension = '.' + path.split('.').pop();
    for (const language of languages) {
      if (language.extensions.includes(extension)) {
        monaco.editor.setModelLanguage(this.editor.getModel(), language.id);
        console.log(`[MONACO]: File extension: ${extension}. Language set to ${language.id}`);
        return;
      }
    }
    monaco.editor.setModelLanguage(this.editor.getModel(), 'text');
  }

  private highlightLines(startLine: number, endLine: number) {
    console.log(startLine, endLine);
    if (!startLine || endLine) {
      return;
    }
    this.decorations = this.editor.deltaDecorations([], [
      {
        range: new monaco.Range(startLine, 1, endLine, 1),
        options: {
          isWholeLine: true,
          className: 'lineHighlightStyle',
          glyphMarginClassName: 'glyphMarginStyle'
        }
      }
    ]);
  }

  private removeHighlight() {
    this.editor.deltaDecorations(this.decorations, []);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}

