import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DiffComponent} from './diff/diff.component';
import {ReviewComponent} from './review.component';
import {LoggedInUserResolver} from '../resolvers/logged-in-user-resolver.service';
import {SubmissionResolver} from './resolvers/submission-resolver.service';
import {SubmissionSourcesComponent} from './submission-sources/submission-sources.component';
import {SubmissionTestsComponent} from './submission-tests/submission-tests.component';
import {FirstSubmissionResolver} from './resolvers/first-submission-resolver.service';
import {SecondSubmissionResolver} from './resolvers/second-submission-resolver.service';
import {NotebooksComponent} from './notebooks/notebooks.component';

const routes: Routes = [
  { path: ':sid',
    component: ReviewComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }},
  { path: ':sid/sources',
    component: SubmissionSourcesComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }},
  { path: ':sid/tests',
    component: SubmissionTestsComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }},
  {
    path: ':sid/notebooks',
    component: NotebooksComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }},
  { path: ':first/:second', component: DiffComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      firstSubmission: FirstSubmissionResolver,
      secondSubmission: SecondSubmissionResolver
    }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewRoutingModule { }
