import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TreeViewService {
  private nodePathSource = new Subject<string>();
  nodePathSent$ = this.nodePathSource.asObservable();

  constructor() { }

  activateNode(path: string) {
    this.nodePathSource.next(path);
  }

}
