import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiffService {

  private firstSubmissionSource = new Subject<string>();
  private secondSubmissionSource = new Subject<string>();
  private selectedFileSource = new Subject<string>();
  firstSubmissionSent$ = this.firstSubmissionSource.asObservable();
  secondSubmissionSent$ = this.secondSubmissionSource.asObservable();
  selectedFileChanged$ = this.selectedFileSource.asObservable();

  constructor() { }

  changeFirstSubmission(id: string) {
    this.firstSubmissionSource.next(id);
  }

  changeSecondSubmission(id: string) {
    this.secondSubmissionSource.next(id);
  }
  changeSelectedFile(path: string) {
    this.selectedFileSource.next(path);
  }
}
