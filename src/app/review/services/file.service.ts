import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  getFile(path: string) {
    return this.http.get(path, { responseType: 'text'})
      .pipe(catchError(FileService.handleError));
  }

  private static handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('A client-side or network error occurred.', error.error.message);
    } else {
      console.error(
        `The backend returned an unsuccessful response code ${error.status}, ` +
        `the response body was: ${error.error}`);
    }
    return throwError(
      error.message);
  }

}
