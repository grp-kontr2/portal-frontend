import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Review, ReviewItem, Submission, User} from '../../shared/models/models';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  submissionsUrl = `${environment.baseUrl}/api/v1.0/submissions`;

  constructor(private http: HttpClient) {
  }

  public getReview(submissionId: string) {
    const url = `${this.submissionsUrl}/${submissionId}/review`;
    return this.http.get<Review>(url);
  }

  /*
  /submission/:sid/review
  POST
   */
  public createReviewItem(submission: Submission, content: string, file: string, startLine: number, endLine: number) {
    console.log(startLine, endLine);
    const url = `${this.submissionsUrl}/${submission.id}/review`;
    const body = {
      'review_items': [
        {
          'file': file,
          'line_start': startLine,
          'line_end': endLine,
          'content': content
        }
      ]
    };
    console.log(`[REVIEW ITEM] Create (${url}): ${body}`);
    return this.http.post(url, body);
  }

  /*
  /submission/:sid/review/:review_item_id
  PUT
   */
  public updateReviewItem(submission: Submission, reviewItem: ReviewItem) {
    const url = `${this.submissionsUrl}/${submission.id}/review/${reviewItem.id}`;
    console.log(reviewItem.content);
    const body = {
      content: reviewItem.content,
      file: reviewItem.file,
      line_start: reviewItem.line_start,
      line_end: reviewItem.line_end
    };
    return this.http.put(url, body);
  }

  /*
  /submission/:sid/review/:review_item_id
  DELETE
   */
  public deleteReviewItem(submission: Submission, reviewItem: ReviewItem) {
    const url = `${this.submissionsUrl}/${submission.id}/review/${reviewItem.id}`;
    return this.http.delete(url);
  }

    /*
  /submission/:sid/review/:review_item_id/versions
  GET
   */
  public getReviewItemVersions(submission: Submission, reviewItem: ReviewItem) {
    const url = `${this.submissionsUrl}/${submission.id}/review/${reviewItem.id}/versions`;
    return this.http.get<ReviewItem[]>(url);
  }
}
