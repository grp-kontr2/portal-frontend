import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotebookService {
  submissionsUrl = `${environment.baseUrl}/api/v1.0/submissions`;

  constructor(private http: HttpClient) {
  }

  public getNotebook(submissionId: string) {
    const url = `${this.submissionsUrl}/${submissionId}/review/is_muni/notepad`;
    return this.http.get(url);
  }

  public postNotebook(submissionId: string, content: string) {
    const url = `${this.submissionsUrl}/${submissionId}/review/is_muni/notepad`;
    const body = {
      'content': content
    };
    return this.http.post(url, body);
  }
}
