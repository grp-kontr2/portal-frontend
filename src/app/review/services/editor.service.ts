import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EditorService {
// https://angular.io/guide/component-interaction#parent-and-children-communicate-via-a-service

  private submissionFilePathSource = new Subject();
  submissionFilePathSent$ = this.submissionFilePathSource.asObservable();

  private testFilePathSource = new Subject();
  testFilePathSent$ = this.testFilePathSource.asObservable();

  private outputFilePathSource = new Subject();
  outputFilePathSent$ = this.outputFilePathSource.asObservable();

  private selectionSource = new Subject<[number, number]>();
  selectionSent$ = this.selectionSource.asObservable();

  private highlightBlockSource = new Subject<[number, number]>();
  highlightBlockSent$ = this.highlightBlockSource.asObservable();

  constructor(private http: HttpClient) { }

  /*
  for submission sources page editor (main)
   */
  sendSubmissionFilePathToEditor(path) {
    this.submissionFilePathSource.next(path);
  }

  /*
  for test sources page editor
   */
  sendTestPathToEditor(path) {
    this.testFilePathSource.next(path);
  }

  /*
  for test output page editor
   */
  sendOutputPathToEditor(path) {
    this.outputFilePathSource.next(path);
  }

  /*
  send selection from editor to create review form
   */
  sendSelectionFromEditor(startLine: number, endLine: number) {
    this.selectionSource.next([startLine, endLine]);
  }

  highlightBlock(startLine: number, endLine: number) {
    this.highlightBlockSource.next([startLine, endLine]);
  }
}
