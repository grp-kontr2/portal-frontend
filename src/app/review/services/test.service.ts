import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  private testInfoSource = new Subject<string>();
  infoSent$ = this.testInfoSource.asObservable();

  constructor() { }

  sendTestInfo(info: string) {
    this.testInfoSource.next(info);
  }
}
