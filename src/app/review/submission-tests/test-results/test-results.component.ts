import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-test-results',
  templateUrl: './test-results.component.html',
  styleUrls: ['./test-results.component.scss']
})
export class TestResultsComponent implements OnInit {
  @Input() tests: any;

  constructor() { }

  ngOnInit() {
  }

}
