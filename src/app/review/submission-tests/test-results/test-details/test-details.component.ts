import {Component, Input, OnInit} from '@angular/core';
import {TestService} from '../../../services/test.service';

@Component({
  selector: 'app-test-details',
  templateUrl: './test-details.component.html',
  styleUrls: ['./test-details.component.scss']
})
export class TestDetailsComponent implements OnInit {
  @Input() tests: any;
  testId: string;
  test;

  constructor(private testService: TestService) { }

  ngOnInit() {
    // sets the first test as default
    if (this.tests && this.tests.length !== 0) {
      this.test = this.tests[0];
    }
    this.testService.infoSent$.subscribe(data => {
      this.testId = data;
      this.getTest(this.testId);
      console.log(this.test);
    });
  }

  private getTest(id: string) {
    for (const item of this.tests) {
      if (item.id === id) {
        this.test = item;
      }
    }
  }
}
