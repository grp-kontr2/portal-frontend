import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {TestService} from '../../../services/test.service';

@Component({
  selector: 'app-tests-table',
  templateUrl: './tests-table.component.html',
  styleUrls: ['./tests-table.component.scss']
})
export class TestsTableComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @Input() tests: any;

  rows = [];
  columns = [
    { name: 'Namespace' },
    { name: 'State' },
    { name: 'Points'}
  ];

  defaultRows = [];
  selected = [];
  filtered = [];

  passedFilterOn = false;
  failFilterOn = false;
  skippedFilterOn = false;
  errorFilterOn = false;

  passed;
  fail;
  skipped;
  error;

  constructor(private testService: TestService) { }

  ngOnInit() {
    if (this.tests) {
      for (const item of this.tests) {
        const row = {
          id: item.id,
          namespace: item.namespace,
          state: item.result.effective.toString(),
          points: item.points
        };
        this.rows.push(row);
      }

      this.defaultRows = this.rows;

      this.passed = this.defaultRows.filter(test => test.state.toLowerCase() === 'pass');
      this.fail = this.defaultRows.filter(test => test.state.toLowerCase() === 'fail');
      this.skipped = this.defaultRows.filter(test => test.state.toLowerCase() === 'none');
      this.error = this.defaultRows.filter(test => test.state.toLowerCase() === 'error');
    }
  }

  pushButton(status: string) {
    if (this[`${status.toLowerCase()}FilterOn`]) {
      this.filterTestsByStatus(status.toLowerCase());
    } else {
      this.removeFilter(status.toLowerCase());
    }
  }

  filterTestsByName(event) {
    const input = event.target.value.toLowerCase();
    // if there are no filters, we search in all tests = defaultRows
    // if there are some restrictions, we search in filtered rows
    const filterSet = this.filtered.length === 0 ?  this.defaultRows : this.filtered;
    const temp = filterSet.filter(function(data) {
      return data.name.toLowerCase().indexOf(input) !== -1 || !input;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  filterTestsByStatus(status: string) {
    const temp = this[`${status.toLowerCase()}`];
    this.filtered = this.filtered.concat(temp);
    this.rows = this.filtered;
    this.table.offset = 0;
  }

  removeFilter(status: string) {
    const temp = this[`${status.toLowerCase()}`];
    this.filtered = this.filtered.filter(element => !temp.includes(element));
    if (this.filtered.length === 0 && !this.passedFilterOn && !this.failFilterOn && !this.skippedFilterOn && !this.errorFilterOn) {
      this.rows = this.defaultRows;
    } else {
      this.rows = this.defaultRows.filter(element => this.filtered.includes(element));
    }
    this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.testService.sendTestInfo(this.selected[0].id);
  }

}
