import { Component, OnInit } from '@angular/core';
import {Submission, User} from '../../shared/models/models';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {SubmissionService} from '../../services/submission.service';
import {PermissionsService} from '../../services/permissions.service';

@Component({
  selector: 'app-submission-tests',
  templateUrl: './submission-tests.component.html',
  styleUrls: ['./submission-tests.component.scss']
})
export class SubmissionTestsComponent implements OnInit {
  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];
  stats: any = null;
  suite: TestSuite = null;
  testSourcesTree: any = null;
  testOutputsTree: any = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      if (data.submission === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
      this.resolveStats();
      this.getTestSourcesTree();
      this.getResultsSourcesTree();
      console.log('[SUB] Submission: ', this.submission);
    }));
  }

  private resolveStats() {
    this.subscriptions.push(this.service.submissionStats(this.submission.id).subscribe(stats => {
      if (stats === null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`No stats available`, { cssClass: 'alert-danger' });
        });
      }
      this.stats = stats;
      console.log('[SUB] Stats: ', this.stats);
      this.parseStats();
    }));
  }

  private parseStats() {
    const parsed = (this.stats);
    const tests: Test[] = [];
    for (const item of parsed.all_tests) {
      const test = new Test(item.name, item.namespace, item.points, item.effective_tags, item.description, item.result, item.tags);
      tests.push(test);
    }
    this.suite = new TestSuite(tests, parsed.result, parsed.tasks_count, parsed.tests_count, parsed.final_points);
    console.log(this.suite);
  }

  private getTestSourcesTree() {
    this.subscriptions.push(this.service.getTree(this.submission.id, 'test_files').subscribe(testFilesTree => {
      this.testSourcesTree = this.parseTree(testFilesTree);
      console.log('[TESTS] Test sources tree: ', this.testSourcesTree);
    }));
  }

  private getResultsSourcesTree() {
    this.subscriptions.push(this.service.getTree(this.submission.id, 'results').subscribe(testFilesTree => {
      this.testOutputsTree = this.parseTree(testFilesTree);
      console.log('[TESTS] Test results tree: ', this.testSourcesTree);
    }));
  }

  private parseTree(tree) {
    const result = [];
    for (const [key, value] of Object.entries(tree)) {
      const node = {
        name: key,
        url: (typeof(value) === 'string') ? value : '',
        content: ''
      };

      if (typeof value === 'string') {
        node['url'] = value;
      } else {
        node['children'] = this.parseTree(value);
      }
      result.push(node);
    }
    return result;
  }

  userCanViewResults() {
    return this.permissions.checkReadResults(this.loggedInUser, this.submission.course);
  }
}

class TestSuite {
  all_tests: Test[];
  result: string;
  tasks_count: any;
  tests_count: any;
  final_points: number;

  constructor(all_tests: any, result: string, tasks_count: any, tests_count: any, final_points: number) {
    this.all_tests = all_tests;
    this.result = result;
    this.tasks_count = tasks_count;
    this.tests_count = tests_count;
    this.final_points = final_points;
  }
}

class Test {
  id: string;
  name: string;
  namespace: string;
  points: number;
  effective_tags: any;
  description: string;
  result: any;
  tags: any;

  constructor(name: string, namespace: string, points: number, effective_tags: any, description: string, result: any, tags: any) {
    this.id = Guid.newGuid();
    this.name = name;
    this.namespace = namespace;
    this.points = points;
    this.effective_tags = effective_tags;
    this.description = description;
    this.result = result;
    this.tags = tags;
  }
}

class Guid {
  // https://stackoverflow.com/questions/26501688/a-typescript-guid-class
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
