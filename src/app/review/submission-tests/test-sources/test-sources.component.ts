import {Component, Input, OnInit} from '@angular/core';
import {TestService} from '../../services/test.service';
import {SubmissionService} from '../../../services/submission.service';
import {Submission, User} from '../../../shared/models/models';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-test-sources',
  templateUrl: './test-sources.component.html',
  styleUrls: ['./test-sources.component.scss']
})

export class TestSourcesComponent implements OnInit {
  @Input() testSourcesTree: any;

  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];

  constructor(private service: TestService, private submissionService: SubmissionService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
    }));
  }

}
