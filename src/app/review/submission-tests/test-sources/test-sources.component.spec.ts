import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSourcesComponent } from './test-sources.component';

describe('TestSourcesComponent', () => {
  let component: TestSourcesComponent;
  let fixture: ComponentFixture<TestSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
