import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestOutputsComponent } from './test-outputs.component';

describe('TestOutputsComponent', () => {
  let component: TestOutputsComponent;
  let fixture: ComponentFixture<TestOutputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestOutputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestOutputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
