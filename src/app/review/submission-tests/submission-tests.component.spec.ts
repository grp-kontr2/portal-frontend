import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmissionTestsComponent } from './submission-tests.component';

describe('SubmissionTestsComponent', () => {
  let component: SubmissionTestsComponent;
  let fixture: ComponentFixture<SubmissionTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmissionTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmissionTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
