import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SubmissionDetailComponent} from './submission-detail/submission-detail.component';
import {SubmissionResolver} from './submission-resolver.service';
import {LoggedInUserResolver} from '../resolvers/logged-in-user-resolver.service';
import {SubmissionListComponent} from './submission-list/submission-list.component';
import {SubmissionCreateComponent} from './submission-create/submission-create.component';
import { SubmissionStatsComponent } from './submission-stats/submission-stats.component';

const routes: Routes = [
  {
    path: '',
    component: SubmissionListComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
    }
  },
  {
    path: 'create',
    component: SubmissionCreateComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: ':sid',
    component: SubmissionDetailComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }
  },
  {
    path: ':sid/resubmit'
  },
  {
    path: ':sid/stats',
    component: SubmissionStatsComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      submission: SubmissionResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmissionsRoutingModule { }
