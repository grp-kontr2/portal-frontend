import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Submission, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PermissionsService } from '../../services/permissions.service';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';
import { AuthService } from '../../services/auth.service';
import { SubmissionService } from '../../services/submission.service';
import { catchError, map } from 'rxjs/operators';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-submission-list',
  templateUrl: './submission-list.component.html',
  styleUrls: [ './submission-list.component.scss' ]
})
export class SubmissionListComponent implements OnInit, OnDestroy {
  loading: boolean;
  loggedInUser: User;
  filters: FormGroup;
  subscriptions: Subscription[] = [];

  @ViewChild('myTable') table: any;

  courseOptions = [];
  groupOptions = [];
  roleOptions = [];
  projectOptions = [];
  stateOptions = [];

  stateSettings: any = { singleSelection: true, text: 'Select State' };
  resultSettings: any = { singleSelection: true, text: 'Select Result' };

  selectedCourse: Course = null;
  selectedGroups = [];
  selectedRoles = [];
  selectedProjects = [];
  selectedUser: string = null;
  selectedState = [];
  temp: Submission[];
  submissions: Submission[];
  resultOptions = [];
  selectedResult = [];

  selected = [];
  selectedSubmissionsAreValid = false;

  queryCourseId: string = null;
  queryProjectId: string = null;
  queryRoleId: string = null;
  queryGroupId: string = null;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private permissions: PermissionsService,
              private userService: UserService,
              private courseService: CourseService,
              private submissionService: SubmissionService,
              private flash: FlashService,
              private auth: AuthService) {
    this.createForm();
    this.loading = true;
  }

  static optionsFromArray(arr: string[]) {
    return arr.map((x) => ({ id: x, itemName: x }));
  }

  async ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;

      this.route.queryParams.subscribe((params) => {
        console.log('[QUERY] Received query params: ', params);
        this.queryCourseId = params[ 'course_id' ];
        this.queryProjectId = params[ 'project_id' ];
        this.queryRoleId = params[ 'role_id' ];
        this.queryGroupId = params[ 'group_id' ];
        console.log(`[QUERY] Course: ${this.queryCourseId}; Project: ${this.queryProjectId}`);
        this.loadSubmissionsForUser();
      });
    }));

    await this.initCourseOptions();
    this.stateOptions = SubmissionListComponent.optionsFromArray([ 'FINISHED', 'CREATED', 'CANCELLED', 'ABORTED', 'IN_PROGRESS' ]);
    this.resultOptions = SubmissionListComponent.optionsFromArray([ 'pass', 'fail', 'error', 'skip', 'none' ]);
    this.onSelect();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.filters = this.fb.group({
      course: [ [], Validators.required ],
      group: [ [] ],
      role: [ [] ],
      project: [ [] ]
    });
  }

  async initCourseOptions() {
    console.log('user courses: ', this.loggedInUser.courses);
    if (this.loggedInUser.courses == null || this.loggedInUser.courses) {
      await this.userService.loadCoursesForUser(this.loggedInUser).toPromise().then(courses => {
        this.loggedInUser.courses = courses;
        this.auth.setLoggedInUser(this.loggedInUser);
        this.courseOptions = courses;
        const id = this.queryCourseId;
        if (id) {
          this.selectedCourse = this.courseOptions.find((course) => course.id === id || course.codename === id);
          this.filters.patchValue({course: this.selectedCourse});
          console.log('[SELECT] Selected course: ', this.selectedCourse);
        }
      });
    }
  }

  initFormOptions() {
    const selectedCourse = this.selectedCourse;
    this.courseService.loadGroupsForCourse(selectedCourse).subscribe(groups => {
      this.groupOptions = groups;
      const id = this.queryGroupId;
      if (id) {
        this.selectedGroups = this.groupOptions.filter((item) => item.id === id || item.codename === id);
        this.filters.patchValue({group: this.selectedGroups});
      }
      console.log('set group options to ', this.groupOptions);
    }, (err) => this.flash.handleError(err));

    this.courseService.loadRolesForCourse(selectedCourse).subscribe(roles => {
      this.roleOptions = roles;
      const id = this.queryRoleId;
      if (id) {
        this.selectedRoles = this.roleOptions.filter((item) => item.id === id || item.codename === id);
        this.filters.patchValue({role: this.selectedRoles});

      }
      console.log('set role options to ', this.roleOptions);
    }, (err) => this.flash.handleError(err));

    this.courseService.loadProjectsForCourse(selectedCourse).subscribe(projects => {
      this.projectOptions = projects;
      const id = this.queryProjectId;
      if (id) {
        this.selectedProjects = this.projectOptions.filter((item) => item.id === id || item.codename === id);
        this.filters.patchValue({projects: this.selectedProjects});

      }
      console.log('set project options to ', this.projectOptions);
    }, (err) => this.flash.handleError(err));
  }

  showFiltered() {
    this.loadSubmissionsForUser();
  }

  onCourseSelect(course: Course) {
    console.log('selected course ', course);
    this.selectedCourse = course;
    this.resetGroupAndRoleOptions();
    this.initFormOptions();
  }

  resetGroupAndRoleOptions() {
    this.groupOptions = [];
    this.selectedGroups = [];
    this.roleOptions = [];
    this.selectedRoles = [];
    this.projectOptions = [];
    this.selectedProjects = [];
    this.loggedInUser.submissions = [];
  }

  loadSubmissionsForUser() {
    const projects = [];
    this.selectedProjects.forEach(({ id }) => {
      projects.push(id);
    });
    // let params = { user: this.selectedUser, course: this.courseId, projects: projects };
    const params = {};
    if (this.selectedUser) {
      params[ 'users' ] = [ this.selectedUser ];
    }
    if (this.selectedCourse) {
      params[ 'course' ] = this.selectedCourse.id;
    }

    if (projects) {
      params[ 'projects' ] = projects;
    }
    this.loading = true;
    this.subscriptions.push(this.submissionService.listSubmissions(params).pipe(
      map(
        (submissions: Submission[]) => {
          const realSubm = submissions.map((item) => {
              item[ 'username' ] = item.user.username;
              return item;
            }
          );
          console.log('in map, got submissions: ', realSubm);
          this.submissions = submissions;
          this.temp = (this.submissions != null) ? [ ...submissions ] : [];
          const text = `Submissions list has been reloaded: ${submissions.length} submissions found`;
          this.flash.flashSuccess(text);
          this.loading = false;
        }
      ), catchError((error: any) => {
        this.flash.handleError(error);
        if (error.status === 403) {
          console.log('in error handler: ', error);
          this.submissions = [];
          return [];
        }
      })).subscribe());
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.submissions = this.temp.filter(function (d) {
      return d.user.username.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onAnySelect(event, selector) {
    const val = event.id.toLowerCase();

    // filter our data
    // update the rows
    this.submissions = this.temp.filter(function (d) {
      return d[ selector ].toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onStateSelect(event: any) {
    this.onAnySelect(event, 'state');
  }

  onResultSelect(event: any) {
    this.onAnySelect(event, 'result');
  }

  onAnyDeselect(event: any) {
    this.submissions = [ ...this.temp ];
  }

  onSelect() {
    this.subscriptions.push(this.submissionService.submissionsSent$.subscribe(submissions => {
      this.selected = submissions;
      this.checkSelected();
    }));
  }

  checkSelected() {
    if (this.selected.length !== 2) {
      this.selectedSubmissionsAreValid = false;
      return;
    }
    const first = (<Submission>this.selected[ 0 ]);
    const second = (<Submission>this.selected[ 1 ]);
    this.selectedSubmissionsAreValid = this.selectionIsValid(first, second);
  }

  selectionIsValid(first: Submission, second: Submission): boolean {
    return first.course.id === second.course.id && first.project.id === second.project.id;
  }
}
