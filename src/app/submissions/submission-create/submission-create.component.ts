import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Course, Project, Submission, User } from '../../shared/models/models';
import { AuthService } from '../../services/auth.service';
import { PermissionsService } from '../../services/permissions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';
import { SubmissionService } from '../../services/submission.service';
import { catchError, map } from 'rxjs/operators';
import { FlashService } from '../../services/flash.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-submission-create',
  templateUrl: './submission-create.component.html',
  styleUrls: [ './submission-create.component.scss' ]
})
export class SubmissionCreateComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  createData: FormGroup;
  subscriptions: Subscription[] = [];

  courseOptions = [];
  projectOptions = [];
  typeOptions = [ 'git' ];
  selectedProject: Project;
  selectedCourse: Course;

  courseId: string = null;
  projectId: string = null;

  verificationString = SubmissionCreateComponent.randomString(5);
  isCreateAllowed = false;


  @ViewChild('itemSelectCourse') itemSelectCourse: NgSelectComponent;
  @ViewChild('itemSelectProject') itemSelectProject: NgSelectComponent;

  static randomString(length): string {
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
  }

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              private fb: FormBuilder,
              private flash: FlashService,
              private permissions: PermissionsService,
              private userService: UserService,
              private courseService: CourseService,
              private cookie: CookieService,
              private submissionService: SubmissionService) {
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngOnInit() {
    const handleRouteData = (data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      this.loadCoursesForSubmission();
    };

    const queryParams = this.route.queryParams.subscribe((params) => {
      console.log('[QUERY] Received route params: ', params);
      this.courseId = params[ 'course_id' ];
      this.projectId = params[ 'project_id' ];
      console.log(`[QUERY] Course id: ${this.courseId}, Project id: ${this.projectId}`);

    });

    this.subscriptions.push(this.route.data.subscribe(handleRouteData));
    this.subscriptions.push(queryParams);
  }

  private loadCoursesForSubmission() {
    this.subscriptions.push(
      this.loadCourseOptions().pipe(map((courses) => {
        console.log('[COURSES] Loaded courses: ', courses);
        this.courseOptions = courses;
        this.selectedCourse = this.checkCourseInOptions();
        this.flash.loadResources(courses, 'courses');
        if (this.selectedCourse) {
          this.loadProjectsBasedOnCourse();
        }
      }), catchError((error: any) => {
        this.flash.flashDanger('Failed to load course options!');
        this.flash.handleError(error);
        return error;
      })).subscribe()
    );
  }

  loadProjectsBasedOnCourse() {
    this.subscriptions.push(
      this.loadProjectOptions(this.selectedCourse).pipe(map((projects) => {
        this.projectOptions = projects;
        this.selectedProject = this.checkProjectInOptions();
        this.flash.loadResources(projects, 'projects');
        this.initFormValues();
      }), catchError((error: any) => {
        const codename = this.selectedCourse.codename;
        this.flash.flashDanger(`Failed to load project options for ${codename}!`);
        this.flash.handleError(error);
        return error;
      })).subscribe()
    );
  }

  private makeGitlabSSHUrl(user: User, course: Course) {
    const base_domain = this.cookie.get('gitlab_domain') || 'gitlab.fi.muni.cz';
    return `git@${base_domain}:${user.gitlab_username}/${course.codename}.git`;
  }


  checkCourseInOptions(): Course {
    if (this.courseOptions.length === 0) {
      return null;
    }
    if (this.courseOptions.length > 0 && !this.courseId) {
      return this.courseOptions[ 0 ];
    }
    return this.courseOptions.find((c: Course) => (c.id === this.courseId || c.codename === this.courseId));
  }

  checkProjectInOptions(): Project {
    if (this.projectOptions.length === 0) {
      return null;
    }
    if (!this.projectId && this.projectOptions.length > 0) {
      return this.projectOptions[ 0 ];
    }
    return this.projectOptions.find((p: Project) => (p.id === this.projectId || p.codename === this.projectId));
  }

  initFormValues() {
    console.log('[FORM] Init: ', this.selectedCourse, this.selectedProject);
    this.createData.patchValue({
      course: this.selectedCourse,
      project: this.selectedProject,
      url: this.makeGitlabSSHUrl(this.loggedInUser, this.selectedCourse),
      from_dir: this.selectedProject.codename,
    }, { emitEvent: false, onlySelf: false });
  }

  createForm() {
    this.createData = this.fb.group({
      course: [ null, Validators.required ],
      project: [ null, Validators.required ],
      type: [ 'git', Validators.required ],
      from_dir: this.fb.control({ value: '', disabled: !this.canUpdateSubmitParams() }),
      url: this.fb.control({ value: '', disabled: !this.canUpdateSubmitParams() }),
      branch: this.fb.control({ value: 'master', disabled: !this.canUpdateSubmitParams() }),
      checkout: this.fb.control({ value: '', disabled: !this.canUpdateSubmitParams() })
    });
  }

  loadCourseOptions(): Observable<Course[]> {
    return this.userService.loadCoursesForUser(this.loggedInUser).pipe(
      map(courses => {
        console.log('[COURSES] Courses for user:', courses);
        return courses.filter((course) => {
          return this.permissions.checkCreateSubmission(this.loggedInUser, course);
        });
      }));
  }

  loadProjectOptions(course: Course): Observable<Project[]> {
    if (!course) {
      return;
    }
    console.log(`loading project options for course ${course.codename}.`);
    this.projectOptions = [];
    this.createData.get('project').reset();

    return this.courseService.loadProjectsForCourse(course).pipe(
      map(projects => {
        this.projectOptions = projects;
        return projects;
      })
    );
  }

  courseChanged(course: Course) {
    if (!course) {
      return;
    }
    console.log('[CHANGE] Course changed: ', course);
    this.projectOptions = [];
    this.createData.get('project').reset();
    this.subscriptions.push(
      this.loadProjectOptions(course).pipe(map((projects) => {
        this.projectOptions = projects;
        this.selectedCourse = course;
        if (projects.length > 0) {
          if (!this.selectedProject) {
            this.selectedProject = projects[ 0 ];
          }
          this.projectChangedPatchData();
        }
      }), catchError((error: any) => {
        this.flash.flashDanger(`Failed to load project options for ${course.codename}!`);
        this.flash.handleError(error);
        return error;
      })).subscribe()
    );
  }

  private projectChangedPatchData() {
    const project = this.selectedProject;
    const course = this.selectedCourse;
    this.createData.patchValue({
      project: project,
      url: this.makeGitlabSSHUrl(this.loggedInUser, course),
      from_dir: project.codename,
    }, { emitEvent: true, onlySelf: false });
  }

  validateGitData(): boolean {
    const data = this.createData.value;
    this.createData.updateValueAndValidity();
    if (data[ 'type' ] === 'git') {
      if (data[ 'url' ] == null || data[ 'url' ] === '') {
        return false;
      }
    }
    return true;
  }

  doCreate() {
    // TODO: project data (same as backend)
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('Submission create form data: ', data);
    const courseId = data[ 'course' ].id;
    const projectId = data[ 'project' ].id;
    const fileParams = {
      source: {
        type: data[ 'type' ],
      },
      from_dir: data[ 'from_dir' ]
    };
    if (data[ 'type' ] === 'git') {
      [ 'url', 'branch', 'checkout' ].forEach((key) => {
        fileParams.source[ key ] = data[ key ];
      });
    }
    const handleError = (error: any) => {
      this.flash.handleError(error);
      this.flash.flashDanger(`You can create new submission: ${error.error.message[ 'when' ]}.`);
    };

    const handleSubmission = (submission: Submission) => {
      this.router.navigateByUrl(`/submissions/${submission.id}`).then(() => {
        this.flash.flashSuccess(`Submission created successfully.`);
      });
    };
    this.subscriptions.push(
      this.submissionService.createSubmission(courseId, projectId, {}, fileParams)
        .subscribe(handleSubmission, handleError)
    );
  }

  projectChanged(project) {
    console.log('[CHANGE] Project has been changed: ', project);
    if (project) {
      this.selectedProject = project;
      this.createData.patchValue({
        from_dir: this.selectedProject.codename
      }, { emitEvent: true, onlySelf: false });
    }
    this.disableOrEnableIfNeeded();
  }

  canUpdateSubmitParams(): boolean {
    if (this.selectedProject == null || this.selectedCourse == null) {
      return true;
    }

    return this.selectedProject.submit_configurable ||
      this.permissions.checkCreateSubmissionOther(this.loggedInUser, this.selectedCourse);
  }

  private disableOrEnableIfNeeded() {
    const git_ctrl = [ 'type', 'from_dir', 'url', 'branch', 'checkout' ];
    const method = (this.canUpdateSubmitParams()) ? 'enable' : 'disable';
    git_ctrl.forEach((key) => {
      const control = this.createData.get(key);
      console.log(`[CONTROL] Control ${key}:`, control);
      control[ method ]();
    });
  }

  randomStringInput(event: any) {

    if (this.canDirectlyCreate()) {
      this.isCreateAllowed = true;
    }

    const val = event.target.value.toLowerCase();
    if (val.trim().toLowerCase() === this.verificationString.trim().toLowerCase()) {
      this.isCreateAllowed = true;
      console.log('Create allowed!');
    } else {
      console.log('Create denied!');
      this.isCreateAllowed = false;
    }
  }

  openCreateModal() {
    this.verificationString = SubmissionCreateComponent.randomString(5);
    this.isCreateAllowed = this.canDirectlyCreate();
    console.log(`Opening modal - create allowed: ${this.isCreateAllowed}`);
  }

  canDirectlyCreate(): boolean {
    return this.permissions.checkCreateSubmissionOther(this.loggedInUser, this.selectedCourse);
  }
}
