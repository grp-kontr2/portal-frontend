import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmissionsRoutingModule } from './submissions-routing.module';
import { SubmissionDetailComponent } from './submission-detail/submission-detail.component';
import {SubmissionResolver} from './submission-resolver.service';
import {SubmissionListComponent} from './submission-list/submission-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {SharedModule} from '../shared/shared.module';
import {SubmissionCreateComponent} from './submission-create/submission-create.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SubmissionStatsComponent } from './submission-stats/submission-stats.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    SubmissionsRoutingModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    SharedModule,
    NgxDatatableModule,
    FormsModule,
    NgSelectModule,
  ],
  providers: [
    SubmissionResolver
  ],
  declarations: [
    SubmissionDetailComponent,
    SubmissionListComponent,
    SubmissionCreateComponent,
    SubmissionStatsComponent
  ]
})
export class SubmissionsModule { }
