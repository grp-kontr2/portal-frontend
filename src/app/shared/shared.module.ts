import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';

import {SideMenuComponent} from './side-menu/side-menu.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { SubmissionsTableComponent } from './submissions-table/submissions-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxDatatableModule,
  ],
  declarations: [
    SideMenuComponent,
    TopMenuComponent,
    SubmissionsTableComponent
  ],
  exports: [
    SideMenuComponent,
    TopMenuComponent,
    SubmissionsTableComponent
  ]
})
export class SharedModule { }
