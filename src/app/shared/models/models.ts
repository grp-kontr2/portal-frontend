// date deserialization problem: https://github.com/angular/angular/issues/21079

import { NamedMixin, TimedMixin } from './mixins';

export class Client extends TimedMixin {
  public id = '';
  public type = '';
  public name = '';
  public codename = '';
  public namespace = '';
}

export class User extends Client {
  public username: string;
  public gitlab_username: string;
  public email: string;
  public uco: number;
  public name: string;
  public is_admin: boolean;
  public managed = false;
  public courses: Course[] = [];
  public roles: Role[] = [];
  public groups: Group[] = [];
  public submissions: Submission[] = [];
  public review_items: ReviewItem[] = [];
  public permissions: object = {};
}

export class Submission extends TimedMixin {
  public note = '';
  public state = '';
  public namespace = '';
  public scheduled_for: string = null;
  public parameters: Object = {};
  public project: Project = null;
  public course: Course = null;
  public user: User = null;
  public source_hash: string = null;
  public username: string = null;
  public result: string = null;
  public points: number = null;
}


export class ReviewItem extends TimedMixin {
  public content = '';
  public file = '';
  public line: number = null;
  public line_start: number = null;
  public line_end: number = null;
  public review: Review = null;
  public user: User = null;
}

export class Review extends TimedMixin {
  public submission: Submission = null;
  public reviewItems: ReviewItem[] = [];
}

export class Course extends NamedMixin {
  public roles: Role[] = [];
  public groups: Group[] = [];
  public projects: Project[] = [];
  public state = '';
}

export class Role extends NamedMixin {
  public clients: Client[] = [];
  public course: Course = null;
  public permissions: Permissions = null;

  public create_submissions = false;
  public create_submissions_other = false;
  public evaluate_submissions = false;
  public handle_notes_access_token = false;
  public read_all_submission_files = false;
  public read_reviews_all = false;
  public read_reviews_groups = false;
  public read_reviews_own = false;
  public read_roles = false;
  public read_submissions_all = false;
  public read_submissions_groups = false;
  public read_submissions_own = false;
  public resubmit_submissions = false;
  public update_course = false;
  public view_course_full = false;
  public view_course_limited = false;
  public write_groups = false;
  public write_projects = false;
  public write_reviews_all = false;
  public write_reviews_group = false;
  public write_reviews_own = false;
  public write_roles = false;
}

export class Permissions extends TimedMixin {
  public create_submissions = false;
  public create_submissions_other = false;
  public evaluate_submissions = false;
  public handle_notes_access_token = false;
  public read_all_submission_files = false;
  public read_reviews_all = false;
  public read_reviews_groups = false;
  public read_reviews_own = false;
  public read_submissions_all = false;
  public read_submissions_groups = false;
  public read_submissions_own = false;
  public resubmit_submissions = false;
  public update_course = false;
  public view_course_full = false;
  public view_course_limited = false;
  public write_groups = false;
  public write_projects = false;
  public write_reviews_all = false;
  public write_reviews_group = false;
  public write_reviews_own = false;
  public write_roles = false;
} // TODO: unused permissions?

export class Group extends NamedMixin {
  public course: Course = null;
  public users: User[] = [];
  public projects: Project[] = [];
}

export class Project extends NamedMixin {
  public assignment_url = '';
  public config: ProjectConfig = null;
  public state = '';
  public course: Course = null;
  public submissions: Submission[] = [];
  public submit_configurable = false;
  public submit_instructions = '';
}

export class ProjectConfig extends TimedMixin {
  public project: Project = null;
  public test_files_source = '';
  public file_whitelist = '';
  public pre_submit_script = '';
  public post_submit_script = '';
  public submission_parameters = '';
  public test_files_subdir = '';
  public submission_scheduler_config = '';
  public submissions_allowed_from: Date = null;
  public submissions_allowed_to: Date = null;
  public archive_from: Date = null;
}

export class Worker extends Client {
  public name = '';
  public url = '';
  public portal_secret = '';
  public tags = '';
}

export class Secret extends TimedMixin {
  public name = '';
  public value = '';
  public expires_at: Date = null;
  public client: Client = null;
}

export class GitlabProject {
  public name = '';
  public description = '';
  public id = '';
  public http_url_to_repo = '';
  public shared_with_groups = [];
  public visibility = '';
  public web_url = '';
  public path = '';
  public path_with_namespace = '';
}


