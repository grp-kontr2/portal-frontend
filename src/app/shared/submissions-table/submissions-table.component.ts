import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Submission, User } from '../models/models';
import { ActivatedRoute, Router } from '@angular/router';
import { SubmissionService } from '../../services/submission.service';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FlashService } from '../../services/flash.service';

@Component({
  selector: 'app-submissions-table',
  templateUrl: './submissions-table.component.html',
  styleUrls: [ './submissions-table.component.scss' ]
})
export class SubmissionsTableComponent implements OnInit, OnDestroy {
  @Input() submissions: Submission[];
  @Input() loggedInUser: User;
  @Input() config: any = {};
  @Input() limit = 5;
  @Input() reloadForUser = false;
  selected = [];
  subscriptions: Subscription[] = [];
  temp: Submission[];

  loadingIndicator = false;
  reorderable = true;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  constructor(private route: ActivatedRoute,
              private submissionService: SubmissionService,
              private flash: FlashService,
              private router: Router) {
  }

  ngOnInit() {
    console.log('user submissions:', this.submissions);
  }

  reloadSubmissions() {
    this.loadingIndicator = true;
    const params = (this.reloadForUser) ? { users: [ this.loggedInUser.id ] } : {};
    const submission_list = this.submissionService.listSubmissions(params).subscribe(
      (submissions: Submission[]) => {
        this.submissions = submissions;
        this.temp = [ ...this.submissions ];
        if (this.reloadForUser) {
          this.loggedInUser.submissions = submissions;
        }
        this.flashLoadResources(submissions, 'submissions');
        this.loadingIndicator = false;
      }, (err) => { this.flash.handleError(err); });
    this.subscriptions.push(submission_list);
  }

  cancelSubmission(submission_id: string) {
    this.subscriptions.push(this.submissionService.cancelSubmission(submission_id).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(
          () => {
            this.flash.flashSuccess('Submission cancelled.');
          }, (error: any) => {
            console.log('Navigation failed: ', error);
            this.flash.flashDanger('Navigation failed, try navigating elsewhere.');
          }
        );
      }, (err) => { this.flash.handleError(err); }
    ));
  }

  private flashLoadResources(courses: any[], entity: string) {
    const text = `Loading ${entity}: ${courses.length} loaded ${entity}`;
    this.flash.flashSuccess(text);
  }


  public getRowClass(row: any) {
    const rowResult = row.result;
    const classResultName = `row-class-${rowResult}`;
    const result = {};
    result[ classResultName ] = true;
    return result;
  }

  onSelect($event) {
    this.submissionService.changeSelectedSubmissions(this.selected);
  }

}
