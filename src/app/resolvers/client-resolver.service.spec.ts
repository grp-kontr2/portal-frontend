import { TestBed, inject } from '@angular/core/testing';

import { ClientResolver } from './client-resolver.service';

describe('ClientResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientResolver]
    });
  });

  it('should be created', inject([ClientResolver], (service: ClientResolver) => {
    expect(service).toBeTruthy();
  }));
});
