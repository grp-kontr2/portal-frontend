import { TestBed, inject } from '@angular/core/testing';

import { CourseQueryParamResolver } from './course-query-param-resolver.service';

describe('CourseQueryParamResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CourseQueryParamResolver]
    });
  });

  it('should be created', inject([CourseQueryParamResolver], (service: CourseQueryParamResolver) => {
    expect(service).toBeTruthy();
  }));
});
