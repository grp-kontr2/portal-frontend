import { TestBed, inject } from '@angular/core/testing';

import { LoggedInUserResolver } from './logged-in-user-resolver.service';

describe('LoggedInUserResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggedInUserResolver]
    });
  });

  it('should be created', inject([LoggedInUserResolver], (service: LoggedInUserResolver) => {
    expect(service).toBeTruthy();
  }));
});
