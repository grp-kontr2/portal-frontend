import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Client } from '../shared/models/models';
import { map, take } from 'rxjs/operators';
import { ClientService } from '../services/client.service';
import { FlashService } from '../services/flash.service';

@Injectable({
  providedIn: 'root'
})
export class ClientResolver {

  constructor(private service: ClientService,
              private router: Router,
              private flash: FlashService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Client> {
    const clientId = route.paramMap.get('clientId');
    return this.service.getClient(clientId).pipe(take(1), map(client => {
      if (client) {
        return client;
      } else {
        this.router.navigateByUrl('/').then(
          () => {
            this.flash.flashDanger(`Client ${clientId} not found.`);
          }, (error: any) => {
            this.flash.flashDanger(`[RESOLVE] Could not navigate to ${route.pathFromRoot}: `, error.error);
            this.flash.handleError(error)
          });
        return null;
      }
    }));
  }
}
