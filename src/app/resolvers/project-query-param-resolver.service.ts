import { Injectable } from '@angular/core';
import {Project} from '../shared/models/models';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ProjectService} from '../services/project.service';
import { take } from 'rxjs/operators';

@Injectable()
export class ProjectQueryParamResolver implements Resolve<Project> {

  constructor(private projectService: ProjectService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> | Promise<Project> {
    const courseId = route.queryParams['cid'];
    const projectId = route.queryParams['pid'];
    if (courseId == null || projectId == null) {
      return null;
    }
    return this.projectService.findProject(courseId, projectId).pipe(take(1));
  }
}
