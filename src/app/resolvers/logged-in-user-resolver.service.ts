import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../shared/models/models';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';
import { take } from 'rxjs/operators';



@Injectable()
export class LoggedInUserResolver implements Resolve<User> {

  constructor(private auth: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    return this.auth.loggedInUser.pipe(take(1));
  }
}
