import { TestBed, inject } from '@angular/core/testing';

import { ProjectQueryParamResolver } from './project-query-param-resolver.service';

describe('ProjectQueryParamResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectQueryParamResolver]
    });
  });

  it('should be created', inject([ProjectQueryParamResolver], (service: ProjectQueryParamResolver) => {
    expect(service).toBeTruthy();
  }));
});
