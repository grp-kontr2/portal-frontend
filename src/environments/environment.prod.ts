export const environment = {
  production: true,
  baseUrl: 'https://kontr.fi.muni.cz',
  baseDomain: 'kontr.fi.muni.cz'
};
